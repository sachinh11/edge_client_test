/*jshint esversion: 8 */
const path = require("path");
const settingsDev = require("./settings.dev");
//Get all the settings
const setting = require("./settings.dev");

module.exports = {
  appName: "SP TCP Handler",
  env: setting.appEnv,
  port: setting.appPort,
  logs: setting.appLog,
  //ip:'localhost',
  ip: setting.appHost,

  root: path.normalize(`${__dirname}/../..`), // root
  base: path.normalize(`${__dirname}/..`), // base

  logFileName: {
    info: "info.log",
    error: "exceptions.log",
  },

  entryControllerIP: setting.entryControllerIP,
  exitControllerIP: setting.exitControllerIP,
  mqttUrl: setting.mqttUrl,
  mqttTopic: setting.mqttTopic,
  siteId: setting.siteId,
  SiteDeviceVersionTopic: settingsDev.SiteDeviceVersionTopic,

  spdb: {
    pg: {
      // PGSQL - Sample URI
      // uri: 'postgres://user:pass@example.com:5432/dbname'
      uri: (() => {
        //If Username Password is set
        if (setting.pgdbIsAuth === "true") {
          return `postgres://${setting.pgdbUsername}:${setting.pgdbPassword}@${setting.pgdbHost}:${setting.pgdbPort}/${setting.pgDbName}`;
        }
        //Without auth
        return `postgres://${setting.pgdbHost}:${setting.pgdbPort}/${setting.pgDbName}`;
      })(),

      masterDb: `${setting.pgDbName}`,
      options: {},
      host: setting.pgdbHost,
      port: setting.pgdbPort,
      username: setting.pgdbUsername,
      password: setting.pgdbPassword,
    },
  },
};
