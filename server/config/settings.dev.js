module.exports = {
  appEnv: process.env.NODE_ENV || "dev",
  appLog: process.env.APP_LOG || "dev",
  appPort: process.env.SP_TCP_HANDLER || 8008,
  appHost: "0.0.0.0",
  entryControllerIP: process.env.SP_ENTRY_CONTROLLER_IP || "127.0.0.1",
  exitControllerIP: process.env.SP_EXIT_CONTROLLER_IP || "127.0.0.1",
  pgdbHost: process.env.SP_EDGE_PGDB_HOST || "localhost",
  pgdbPort: process.env.SP_EDGE_PGDB_PORT || "54323",
  pgdbIsAuth: process.env.SP_EDGE_PGDB_IS_AUTH || "true",
  pgdbUsername: process.env.SP_EDGE_PGDB_USERNAME || "master",
  pgdbPassword: process.env.SP_EDGE_PGDB_PASSWORD || "DHNNOQIYWMDZZPOQ",
  pgDbName: process.env.SP_EDGE_PGDB_NAME || "EV3",
  mqttUrl: process.env.MQTTURL || "mqtt://iot.hyperthings.in:17004",
  mqttTopic: process.env.MQTT_TOPIC || "915-test-topic1",
  siteId: process.env.siteId || "915",
  SiteDeviceVersionTopic:
    process.env.SiteDeviceVersionTopic || "915-device-version",
};
