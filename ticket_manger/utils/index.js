//Import Axios
const Axios = require("axios");
//Import mqtt
const mqtt = require("mqtt");
//Import moment
const moment = require("moment");
//Import Settings
const { mqttUrl, mqttTopic } = require("../config/adaptor");
//connect to mqtt client
const client = mqtt.connect(mqttUrl);

//All Utils
class Utils {
  //log Publisher
  async logPublisher(servName, logLevel, source, msg) {
    // console.log("Here!!");
    try {
      if (servName && logLevel && source && msg) {
        if (servName == 1) {
          servName = "TCP-server";
        } else if (servName == 2) {
          servName = "Ticket-Manager";
        } else {
          servName = "unknown!";
        }
        if (logLevel == 1) {
          logLevel = "Success";
        } else if (logLevel == 2) {
          logLevel = "Error";
        } else if (logLevel == 3) {
          logLevel = "Crashed";
        } else {
          logLevel = "unknown!";
        }
        if (source == 1) {
          source = "EntryGate";
        } else if (source == 2) {
          source = "ExitGate";
        } else if (source == 3) {
          source = "server";
        } else if (source == 4) {
          source = "DB";
        } else {
          source = "unknown";
        }
        let pubObj = {};
        pubObj.time = moment().format();
        pubObj.serviceNeme = servName;
        pubObj.logLevel = logLevel;
        pubObj.source = source;
        pubObj.mgs = msg;
        // console.log(pubObj);

        await this.pushdata(mqttTopic, JSON.stringify(pubObj));
      }
    } catch (error) {
      console.log("cannot capture log", error);
    }
  }

  pushdata(topic, data) {
    return new Promise((resolve) => {
      client.publish(topic, data);
      resolve("push data");
    });
  }
  //call entry image
  async entryImage(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:8005/data/entry_image`,
        headers: {},
        data,
      });
      console.log("2=====> Response from entry_image", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }

  //call exit image
  async exitImage(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:8005/data/exit_image`,
        headers: {},
        data,
      });
      console.log("2=====> Response from exit_image", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }
}
module.exports = Utils;
