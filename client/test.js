//Import Configs
const {
  spdb,
  entryControllerIP,
  exitControllerIP,
} = require('./config/adaptor');
(async () => {
  try {
    const dbclient = new Client(spdb.pg.uri);
    try {
      await dbclient.connect();
      console.log('DataBase connected!');
    } catch (e) {
      console.log('DataBase Not connected', e);
    }
    // Build and instantiate our custom event emitter
    function DbEventEmitter() {
      EventEmitter.call(this);
    }
    util.inherits(DbEventEmitter, EventEmitter);
    const dbEventEmitter = new DbEventEmitter();
    // Handle New devices
    dbEventEmitter.on('new_command', async (msg) => {
      // Fetch command
      let new_cmd = await dbclient.query(
        `select * from commands where id = ${msg.id}`
      );
      //Create the client using Socket method.
      const client = new net.Socket();
      new_cmd = new_cmd.rows[0];
      //Now that client is created, let's make it connect to the server and send some message. So that server can echo back the same to all clients
      client.connect(new_cmd.port, new_cmd.ip, async () => {
        console.log(`client connected to ${new_cmd.ip}:${new_cmd.port}`);
        let cmd_str = '';
        if (new_cmd.cmd_type == '10') {
          //Open Barrier
          cmd_str = '7F01004404010120031DED';
          console.log('Open Barrier', cmd_str);
          //Create the client using Socket method.
          const client1 = new net.Socket();
          client1.connect(8008, '0.0.0.0', () => {
            if (new_cmd.ip == entryControllerIP) {
              const str = '01';
              //Send Command
              const bufferRes = Buffer.from(str, 'hex');
              client1.write(bufferRes);
              client1.destroy();
            }
          });
        } else if (new_cmd.cmd_type == '11') {
          await new Promise((resolve) => setTimeout(resolve, 2000));
          //Close Barrier
          cmd_str = '7F02004404020120031DEF';
          console.log('Close Barrier', cmd_str);
        } else if (new_cmd.cmd_type == 20) {
          //Convert pass_id to hex
          let pass_id_hex = Utils.asciiToHex(new_cmd.data.pass_id);
          let temp_0 = '';
          for (let i = 0; i < 48 - pass_id_hex.length; i++) {
            temp_0 = temp_0 + '0';
          }
          pass_id_hex += temp_0;
          //Hex of URL
          let url_hex = Utils.asciiToHex(new_cmd.data.url);
          url_hex += '00';
          cmd_str = `7F03007154${pass_id_hex}${url_hex}`;
          const xor = Utils.computeXor(cmd_str);
          const checksum = Utils.computeChecksum(cmd_str);
          cmd_str += xor + checksum;
          console.log('Print ticket', cmd_str);
        }
        //Display text
        else if (new_cmd.cmd_type == 30) {
          //Convert text to hex
          let text_hex = Utils.asciiToHex(new_cmd.data.text);
          text_hex = `0050${text_hex}00`;
          let text_hex_len = text_hex.length / 2;
          text_hex_len = text_hex_len.toString(16);
          cmd_str = `7F030073${text_hex_len}${text_hex}`;
          const xor = Utils.computeXor(cmd_str);
          const checksum = Utils.computeChecksum(cmd_str);
          cmd_str += xor + checksum;
          console.log('Display Command', cmd_str);
        } else if (new_cmd.cmd_type == 50) {
          cmd_str = '7F01001905010205030463AD';
          console.log('POS Command trigger', cmd_str);
        } else if (new_cmd.cmd_type == 51) {
          const pos_text = Utils.computePosCmd({
            MSGID: 'PUR',
            ECRNO: '001',
            RecieptNo: new_cmd.data.invoice_id,
            Amount: new_cmd.data.amount,
          });
          cmd_str = '7F0300192601' + pos_text;
          cmd_str =
            cmd_str +
            Utils.computeXor(cmd_str) +
            Utils.computeChecksum(cmd_str);
          console.log('POS Money Command', cmd_str);
        } else {
          console.log('Unsupported command type', cmd_type);
          return false;
        }
        //Send Command
        const bufferRes = Buffer.from(cmd_str, 'hex');
        const res = client.write(bufferRes);
        console.log('Response from controller', res);
      });
      //Lets listen to some events similar to server like data, close and error.
      client.on('data', (rawData) => {
        const data = Buffer.from(rawData).toString('hex');
        console.log(`Client received: ${data}`);
        client.destroy();
      });
      // Add a 'close' event handler for the client socket
      client.on('close', () => {
        console.log('Client closed');
      });
      client.on('error', (err) => {
        console.error(err);
      });
    });
    // Listen for all pg_notify channel messages
    dbclient.on('notification', function (msg) {
      let payload = JSON.parse(msg.payload);
      dbEventEmitter.emit(msg.channel, payload);
    });
    // Designate which channels we are listening on. Add additional channels with multiple lines.
    await dbclient.query('LISTEN new_command');
  } catch (e) {
    console.log(e);
  }
})();
