//Import Net
const Net = require("net");

//Import Node PG
const { Client } = require("pg");

let client = null;

//Import Config
const {
  appName,
  port,
  ip,
  entryControllerIP,
  exitControllerIP,
  tcpClientIP,
  spdb,
  siteId,
  SiteDeviceVersionTopic,
} = require("./config/adaptor");
//Import Utility functions
const UtilClass = require("./utils");
const Utils = new UtilClass();
/*

*/

//Create TCP Server
const server = Net.createServer();

//Start Server
server.listen(port, ip, () => {
  console.log(`${appName} listening on ${ip}:${port}`);
});
Utils.logPublisher(1, 1, 3, `${appName} listening on ${ip}:${port}`);
//db connection
(async () => {
  client = new Client(spdb.pg.uri);
  try {
    await client.connect();
    console.log("DataBase connected!");
    Utils.logPublisher(1, 1, 4, `DataBase connected!`);
  } catch (e) {
    console.log("DataBase Not connected", e);
    Utils.logPublisher(1, 2, 4, `DataBase Not connected!`);
  }
})();

//Ground Sense Change Bit previous state
//Ground sense - change bit - card fetch
let cf_flag = true;
//RFID data flag
let rfid_flag = true;
//Exit RFID
let ex_rfid_flag = true;
//GS 1
let db_org_2b_bin_gs1 = 0;
let cb_org_2b_bin_cf = 0;
let cb_org_1b_bin_gs2 = 0;
let ex_db_org_2b_bin_gs1 = 0;
let ex_cb_org_1b_bin_gs2 = 0;
let ext_cb_org_2b_bin_cf = 0;
//Open Entry Barrier flag
let o_ent_barrier = true;
//Open Exit Barrier flag
let o_ext_barrier = true;
//Entry ground sense
let ent_gs = false;
// occupancy count
let g_carCount = 100;

//generated ticket passid
let G_Pass_id = {
  pass_id: 0,
  passFlag: false,
  rfid_flag: false,
  customerFlag: false,
};
//generate ticket rfid
let G_Exit_Pass_id = {
  pass_id: 0,
  passFlag: false,
  rfid_flag: false,
  customerFlag: false,
};
//Get all sockets
let sockets = [];

let terminalData1 = {
  entryterminalData: "",
  exitterminalData: "",
};

(async () => {
  const query1 = "select terminal_ip_address from devices where uid = '2'";
  const terminalData = await client.query(query1);
  terminalData1.entryterminalData = terminalData.rows[0].terminal_ip_address;
  terminalData1.entryBit = terminalData.rows[0].terminal_lock;
  console.log("Entry Ip", terminalData.rows[0].terminal_ip_address);

  //Get Exit Controller Ip address
  const query2 = "select terminal_ip_address from devices where uid = '1'";
  const exitterminalData = await client.query(query2);
  terminalData1.exitterminalData = exitterminalData.rows[0].terminal_ip_address;
  terminalData1.exitBit = exitterminalData.rows[0].terminal_lock;
  console.log("Exit Ip", exitterminalData.rows[0].terminal_ip_address);
})();

server.on("connection", (socket) => {
  const clientAddress = `${socket.remoteAddress}:${socket.remotePort}`;
  console.log(new Date(), `New Device connected: ${clientAddress}`);
  Utils.logPublisher(
    1,
    1,
    3,
    `${new Date()},New Device connected: ${clientAddress}`
  );
  socket.on("data", async (rawData) => {
    //check device status is online

    const query10 = `Select is_online from devices WHERE terminal_ip_address='${socket.remoteAddress}'`;

    onlineData = await client.query(query10);
    if (onlineData.rows[0].is_online == true) {
      try {
        //
        //Convert to HEX
        const data = Buffer.from(rawData).toString("hex");
        // rawData.toString()
        console.log(data);

        if (data.length == 2) {
          console.log("TCP Client Open Barrier Flag", data);
          if (data == "01") {
            o_ent_barrier = true;
            // cf_flag = true;
          } else if (data == "11") {
            o_ext_barrier = true;
          }
        } else {
          const stx = data.slice(0, 2);
          const seq = data.slice(2, 4);
          const state = data.slice(4, 6);
          const command = data.slice(6, 8);
          console.log(command);

          const dataLen = parseInt(data.slice(8, 10), 16);
          const packet = data.slice(10, 2 * dataLen + 10);
          const xor = data.slice(10 + 2 * dataLen, 10 + 2 * dataLen + 2);
          const checksum = data.slice(
            10 + 2 * dataLen + 2,
            10 + 2 * dataLen + 4
          );

          if (command != 33) {
            console.log(
              `Input ------ (data -> ${data}, stx -> ${stx}, seq -> ${seq}, state ${state}, command -> ${command} dataLen ${dataLen}, packet -> ${packet}, xor -> ${xor}, checksum -> ${checksum})`
            );

            //Prepare ACK CMD
            // adding 0x80 = 128 decimal to command by using decimal value
            const ack_cmd_type = (parseInt(command, 16) + 128).toString(16),
              ack_data_len = "00";
            let ack_str = `${stx}${seq}${state}${ack_cmd_type}${ack_data_len}`,
              ack_res = "";
            //Get Xor and Checksum
            const ack_xor = Utils.computeXor(ack_str);
            const ack_checksum = Utils.computeChecksum(ack_str);
            ack_str = `${ack_str}${ack_xor}${ack_checksum}`;
            //Convert to Buffer from string
            const bufferRes = Buffer.from(ack_str, "hex");

            //Check if data is coming form entry or exit gate
            //Entry gate
            if (socket.remoteAddress == terminalData1.entryterminalData) {
              console.log("---Entry Gate data---");
              // Save raw data log
              try {
                const raw_log = await Utils.saveRawData({
                  gate: 1,
                  data: data,
                });
                console.log("Success", raw_log.response);
              } catch (err) {
                console.log("Error", err.response);
              }
              switch (command) {
                case "43":
                  const db_org = packet.slice(0, 8);
                  const cb_org = packet.slice(8, 16);
                  console.log("Data Bits", db_org);
                  console.log("Change Bits", cb_org);
                  const db_org_1b = db_org.slice(0, 2);
                  const db_org_2b = db_org.slice(2, 4);

                  const cb_org_1b = cb_org.slice(0, 2);
                  const cb_org_2b = cb_org.slice(2, 4);

                  const db_org_1b_bin = Utils.hex2bin(db_org_1b);
                  const db_org_2b_bin = Utils.hex2bin(db_org_2b);
                  const cb_org_1b_bin = Utils.hex2bin(cb_org_1b);
                  const cb_org_2b_bin = Utils.hex2bin(cb_org_2b);

                  const db_org_1b_bin_gs2 = db_org_1b_bin.charAt(1);
                  db_org_2b_bin_gs1 = db_org_2b_bin.charAt(4);
                  const db_org_2b_bin_cf = db_org_2b_bin.charAt(7);

                  console.log(
                    "Data Bits - GS1, GS2, CF",
                    db_org_2b_bin_gs1,
                    db_org_1b_bin_gs2,
                    db_org_2b_bin_cf
                  );

                  cb_org_1b_bin_gs2 = cb_org_1b_bin.charAt(1);
                  const cb_org_2b_bin_gs1 = cb_org_2b_bin.charAt(4);
                  cb_org_2b_bin_cf = cb_org_2b_bin.charAt(7);

                  console.log(
                    "Change Bits - GS1, GS2, CF",
                    cb_org_2b_bin_gs1,
                    cb_org_1b_bin_gs2,
                    cb_org_2b_bin_cf
                  );
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  //Get change bit
                  let change_bit = packet.slice(10, 12);
                  change_bit = Utils.hex2bin(change_bit);
                  //Card fetch
                  const card_fetch = change_bit.charAt(7);
                  //Ground sense
                  const ground_sense = change_bit.charAt(4);
                  //Ground sense 2
                  let gs = packet.slice(2, 4);
                  gs = Utils.hex2bin(gs);
                  gs = gs.charAt(4);
                  //Ground sense true
                  if (ground_sense == 1) {
                    ent_gs = true;
                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                    console.log("___GS1 Occurring__");
                    Utils.logPublisher(1, 1, 1, `CAR Detected`);
                    //Send Command to Scan/Press Button
                    try {
                      const sendCommand = await Utils.sendCommands({
                        gate: "in",
                        cmd_type: "33",
                      });
                      console.log("Success", sendCommand.response);
                    } catch (err) {
                      console.log("Error", err.response);
                    }
                  } else {
                    ent_gs = false;
                  }
                  console.log(
                    "========== 1",
                    db_org_2b_bin_gs1,
                    cb_org_2b_bin_cf,
                    cf_flag
                  );
                  //Card Fetch - true
                  if (card_fetch == 1) {
                    if (
                      db_org_2b_bin_gs1 == 1 &&
                      cb_org_2b_bin_cf == 1 &&
                      cf_flag
                    ) {
                      console.log("Card Fetch pressed");
                      Utils.logPublisher(1, 1, 1, `Card Fetch pressed`);
                      cf_flag = false;
                      console.log(
                        "========== 2",
                        db_org_2b_bin_gs1,
                        cb_org_2b_bin_cf,
                        cf_flag
                      );
                      //Create regular parking
                      try {
                        let genTicket = await Utils.genRegularParking();
                        G_Pass_id.pass_id = genTicket.pass_id;
                        G_Pass_id.passFlag = true;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = false;
                        console.log("Success", genTicket.status);
                        console.log("here");
                      } catch (err) {
                        console.log("Error", err.response);
                      }
                    } else {
                      console.log("Duplicate Card Fetch pressed");
                    }
                  }
                  //Ground Sense 2 is occurring
                  // if (cb_org_1b_bin_gs2 == 1 || db_org_2b_bin_gs1 == 0) {

                  if (
                    cb_org_1b_bin_gs2 == 1 ||
                    (db_org_2b_bin_gs1 == 0 && cb_org_2b_bin_gs1 == 1)
                  ) {
                    console.log("___Clearing the Screen__");
                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                    cf_flag = true;
                    rfid_flag = true;
                    try {
                      await Utils.sendCommands({
                        cmd_type: "31",
                        gate: "in",
                      });
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  if (cb_org_1b_bin_gs2 == 1) {
                    Utils.logPublisher(1, 1, 1, `CAR Entered`);
                    await Utils.updateTicket({ G_Pass_id });
                    G_Pass_id.passFlag = false;
                    G_Pass_id.pass_id = 0;
                    G_Pass_id.rfid_flag = false;
                    G_Pass_id.customerFlag = false;

                    if (g_carCount > 0 && g_carCount <= 100) {
                      g_carCount = g_carCount - 1;
                    }
                    console.log("___car passed____CarCount : ", g_carCount);
                    //Get data from sites
                    const query = "select * from sites";
                    const test = await client.query(query);

                    //Check the slot details
                    let data = {
                      no_of_slots: test.rows[0].no_of_slots,
                      available_slots: test.rows[0].available_slots,
                      occupied_slots: test.rows[0].occupied_slots,
                    };
                    console.log(data);

                    //Validate available slot is null or not
                    if (data.available_slots == 0 || null) {
                      data.available_slots = data.no_of_slots;
                    } else {
                      data.available_slots = data.available_slots - 1;
                    }

                    //Validate Occupied slot is null or not
                    if (data.occupied_slots == 0 || null) {
                      data.occupied_slots = 0;
                    } else {
                      data.occupied_slots = data.occupied_slots + 1;
                    }

                    console.log(
                      "Data",
                      data.available_slots,
                      data.occupied_slots
                    );

                    //Update site with the slot details
                    const query1 = `UPDATE SITES SET available_slots=${data.available_slots}, occupied_slots=${data.occupied_slots} where id=912`;
                    await client.query(query1);

                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                  }
                  console.log(`State Change in ${data}, ACK Out ${ack_str}`);
                  break;

                case "33":
                  ack_res = socket.write(bufferRes);
                  console.log(`HeartBeat in ${data}, ACK Out ${ack_str}`);
                  break;
                case "1a":
                  console.log("GS1, CF", db_org_2b_bin_gs1, cf_flag);
                  if (db_org_2b_bin_gs1 == 1 && cf_flag) {
                    const tempData = Utils.h2a(
                      packet.slice(2, packet.length - 4)
                    );
                    if (tempData.includes("https")) {
                      const passArr = tempData.split("/");
                      const passId = passArr[passArr.length - 1];
                      Utils.logPublisher(1, 1, 1, `Reservation Ticket scanned`);
                      console.log(
                        "Open Ticket for Pass ID",
                        passArr[passArr.length - 1]
                      );
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const genTicket = await Utils.genTicket({
                          pass_id: passId,
                        });
                        G_Pass_id.pass_id = passId;
                        G_Pass_id.passFlag = true;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = false;

                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    } else {
                      const tempData = Utils.h2a(
                        packet.slice(0, packet.length - 4)
                      );
                      const customerId = tempData;
                      Utils.logPublisher(
                        1,
                        1,
                        1,
                        `Customer QR code scanned ${customerId}`
                      );
                      console.log(customerId, "++++");
                      console.log("Open Ticket for Customer ID", customerId);
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const genTicket = await Utils.genTicket({
                          customer_id: customerId,
                        });
                        console.log(genTicket.msg);
                        G_Pass_id.pass_id = genTicket.msg;
                        G_Pass_id.passFlag = false;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = true;

                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  }
                  break;
                case "11":
                  let rfid = packet.slice(2, packet.length - 4);
                  rfid = await Utils.revHex(rfid);
                  rfid = parseInt(rfid, 16);
                  ack_res = socket.write(bufferRes);
                  console.log(`RFID in ${data}, ACK Out ${ack_str}, ${rfid}`);
                  Utils.logPublisher(1, 1, 1, `RFID Scanned ${data}`);

                  if (db_org_2b_bin_gs1 == 1 && rfid_flag) {
                    rfid_flag = false;
                    console.log("Duplicate RFID");
                    try {
                      const genTicket = await Utils.genTicket({
                        rfid: `${rfid}`,
                      });
                      G_Pass_id.pass_id = rfid;
                      G_Pass_id.passFlag = false;
                      G_Pass_id.rfid_flag = true;
                      console.log("Success", genTicket.response);
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  break;
                case "82":
                  console.log(data);
                  let sac = data.slice(10, data.length - 4);
                  let deviceVersion = Utils.h2a(sac);
                  let deviceData = deviceVersion.split("V");
                  dataObj = {};
                  dataObj.siteId = siteId;
                  dataObj.deviceData = deviceVersion;

                  Utils.pushdata(
                    `${SiteDeviceVersionTopic}`,
                    JSON.stringify(dataObj)
                  );

                  console.log("Data for Device Version", deviceData[0]);
                  break;
                default:
                  console.log("Invalid Command !");
              }
            }
            //Exit gate
            else if (socket.remoteAddress == terminalData1.exitterminalData) {
              //Exit Gate
              console.log("---Exit Gate data---");
              //Save raw data log
              try {
                const raw_log = await Utils.saveRawData({
                  gate: 2,
                  data: data,
                });
                console.log("Success", raw_log.response);
              } catch (err) {
                console.log("Error", err.response);
              }
              switch (command) {
                case "44":
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  console.log(`NEW in ${data}, ACK Out ${ack_str}`);

                  break;
                case "43":
                  const db_org = packet.slice(0, 8);
                  const cb_org = packet.slice(8, 16);
                  console.log("Exit Data Bits", db_org);
                  console.log("Exit Change Bits", cb_org);
                  const db_org_1b = db_org.slice(0, 2);
                  const db_org_2b = db_org.slice(2, 4);

                  const cb_org_1b = cb_org.slice(0, 2);
                  const cb_org_2b = cb_org.slice(2, 4);

                  const db_org_1b_bin = Utils.hex2bin(db_org_1b);
                  const db_org_2b_bin = Utils.hex2bin(db_org_2b);
                  const cb_org_1b_bin = Utils.hex2bin(cb_org_1b);
                  const cb_org_2b_bin = Utils.hex2bin(cb_org_2b);

                  const db_org_1b_bin_gs2 = db_org_1b_bin.charAt(1);
                  ex_db_org_2b_bin_gs1 = db_org_2b_bin.charAt(4);
                  const db_org_2b_bin_cf = db_org_2b_bin.charAt(7);

                  console.log(
                    "Data Bits - GS1, GS2, CF",
                    ex_db_org_2b_bin_gs1,
                    db_org_1b_bin_gs2,
                    db_org_2b_bin_cf
                  );

                  ex_cb_org_1b_bin_gs2 = cb_org_1b_bin.charAt(1);
                  const cb_org_2b_bin_gs1 = cb_org_2b_bin.charAt(4);
                  ext_cb_org_2b_bin_cf = cb_org_2b_bin.charAt(7);

                  console.log(
                    "Change Bits - GS1, GS2, CF",
                    cb_org_2b_bin_gs1,
                    ex_cb_org_1b_bin_gs2,
                    ext_cb_org_2b_bin_cf
                  );
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  //Get change bit
                  let change_bit = packet.slice(10, 12);
                  change_bit = Utils.hex2bin(change_bit);

                  let ground_sence = change_bit.charAt(4);
                  //Ground sense true
                  if (ground_sence == 1) {
                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                    console.log("Ground sense true");
                    Utils.logPublisher(1, 1, 2, "CAR Detected");
                    //Create regular parking
                    try {
                      const sendCommand = await Utils.sendCommands({
                        gate: "out",
                        cmd_type: "34",
                      });
                      console.log("Success", sendCommand.response);
                    } catch (err) {
                      console.log("Error", err.response);
                    }
                  }
                  if (ex_cb_org_1b_bin_gs2 == 1) {
                    ex_rfid_flag = true;
                    console.log("________GS2 Occurring_______");
                    Utils.logPublisher(1, 1, 2, `CAR Exit`);
                    try {
                      // await Utils.sendCommands({
                      //   cmd_type: "11",
                      //   gate: "in",
                      // });
                      // await Utils.sendCommands({
                      //   cmd_type: "31",
                      //   gate: "out",
                      // });
                      await Utils.exitUpdateTicket({
                        G_Exit_Pass_id,
                      });

                      G_Exit_Pass_id.pass_id = 0;
                      G_Exit_Pass_id.rfid_flag = false;
                      G_Exit_Pass_id.passFlag = false;
                      G_Exit_Pass_id.customerFlag = false;
                      if (g_carCount > 0 && g_carCount != 100) {
                        g_carCount = g_carCount + 1;
                      }
                      console.log("CarCount : ", g_carCount);

                      //Get data from sites
                      const query = "select * from sites";
                      const test = await client.query(query);

                      //Get Slot details
                      let data = {
                        no_of_slots: test.rows[0].no_of_slots,
                        available_slots: test.rows[0].available_slots,
                        occupied_slots: test.rows[0].occupied_slots,
                      };
                      console.log(data);

                      //Check available slot is there or not
                      if (data.available_slots == 0 || null) {
                        data.available_slots = data.no_of_slots;
                      } else {
                        data.available_slots = data.available_slots + 1;
                      }

                      //Check occupied slot is there or not
                      if (data.occupied_slots == 0 || null) {
                        data.occupied_slots = 0;
                      } else {
                        data.occupied_slots = data.occupied_slots - 1;
                      }

                      console.log(
                        "Data",
                        data.available_slots,
                        data.occupied_slots
                      );

                      //Update the site with slot details
                      const query1 = `UPDATE SITES SET available_slots=${data.available_slots}, occupied_slots=${data.occupied_slots} where id=912`;
                      await client.query(query1);
                    } catch (err) {
                      console.log("Error", err);
                    }

                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                  }
                  //Ground Sense 2 is occurring
                  if (
                    ex_cb_org_1b_bin_gs2 == 1 ||
                    (ex_db_org_2b_bin_gs1 == 0 && cb_org_2b_bin_gs1 == 1)
                  ) {
                    ex_rfid_flag = true;
                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                    console.log("________Clearing the screen _______");
                    try {
                      // await Utils.sendCommands({
                      //   cmd_type: "11",
                      //   gate: "in",
                      // });
                      G_Exit_Pass_id.pass_id = 0;
                      G_Exit_Pass_id.rfid_flag = false;
                      G_Exit_Pass_id.passFlag = false;
                      G_Exit_Pass_id.customerFlag = false;
                      await Utils.sendCommands({
                        cmd_type: "31",
                        gate: "out",
                      });
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }

                  console.log(`State Change in ${data}, ACK Out ${ack_str} `);
                  break;
                case "33":
                  ack_res = socket.write(bufferRes);
                  console.log(`HeartBeat in ${data}, ACK Out ${ack_str}`);
                  break;
                case "19":
                  if (packet.slice(0, 2) == "01") {
                    let rfid = Utils.h2a(packet.slice(2, packet.length - 4));
                    rfid = rfid.slice(-5);
                    rfid = parseInt(rfid, 16);
                    ack_res = socket.write(bufferRes);
                    console.log(
                      `RFID Out ${data}, ACK Out ${ack_str}, ${rfid}`
                    );
                    if (ex_db_org_2b_bin_gs1 == 1 && ex_rfid_flag) {
                      ex_rfid_flag = false;
                      try {
                        const genTicket = await Utils.closeTicket({
                          rfid: `${rfid}`,
                        });
                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  } else if (packet.slice(0, 2) == "02") {
                    if (G_Exit_Pass_id.passFlag == true) {
                      const data = await Utils.posParser(packet);
                      console.log(data);
                      if (data.status == "000") {
                        try {
                          // const sendCommand = await Utils.sendCommands({
                          //   gate: "out",
                          //   cmd_type: "30",
                          //   text: "Payment Failed",
                          // });
                          console.log("Payment Successfull");

                          await Utils.capturePayment({
                            passId: G_Exit_Pass_id.pass_id,
                            status: 1,
                            recepit_no: data.recepit_no,
                            transaction_no: data.transaction_no,
                          });
                        } catch (err) {
                          console.log("Error", err.response);
                        }
                      } else {
                        try {
                          console.log("Payment Cancel");
                          Utils.logPublisher(1, 1, 2, `Payment Cancel`);
                          // const sendCommand = await Utils.sendCommands({
                          //   gate: "out",
                          //   cmd_type: "32",
                          // });
                          // await new Promise((resolve) =>
                          //   setTimeout(resolve, 500)
                          // );

                          console.log("PassId", G_Exit_Pass_id.pass_id);
                          await Utils.capturePayment({
                            passId: G_Exit_Pass_id.pass_id,
                            status: 0,
                          });
                          // await Utils.sendCommands({
                          //   cmd_type: "10",
                          //   gate: "out",
                          // });

                          console.log("Success", sendCommand.response);
                        } catch (err) {
                          console.log("Error", err.response);
                        }
                      }
                    }
                  }

                  break;
                case "1a":
                  if (ex_db_org_2b_bin_gs1 == 1) {
                    const tempData = Utils.h2a(
                      packet.slice(2, packet.length - 4)
                    );

                    if (tempData.includes("https")) {
                      const passArr = tempData.split("/");
                      const passId = passArr[passArr.length - 1];
                      console.log(
                        "Close Ticket for Pass ID",
                        passArr[passArr.length - 1]
                      );
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket scanned for passId: ${passId}`
                      );
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const closeTicket = await Utils.closeTicket({
                          pass_id: passId,
                        });
                        G_Exit_Pass_id.pass_id = passId;
                        G_Exit_Pass_id.rfid_flag = false;
                        G_Exit_Pass_id.passFlag = true;
                        G_Exit_Pass_id.customerFlag = false;
                        console.log("Success", closeTicket.response);
                      } catch (err) {
                        console.log("Error", err.response);
                      }
                    } else {
                      const tempData = Utils.h2a(
                        packet.slice(0, packet.length - 4)
                      );
                      const customerId = tempData;
                      console.log(customerId, "++++");
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket scanned for customerId: ${customerId}`
                      );
                      console.log("Close Ticket for Customer ID", customerId);
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const closeTicket = await Utils.closeTicket({
                          customer_id: customerId,
                        });
                        console.log("closeTicket", closeTicket.id);
                        G_Exit_Pass_id.pass_id = closeTicket.id;
                        G_Exit_Pass_id.passFlag = false;
                        G_Exit_Pass_id.rfid_flag = false;
                        G_Exit_Pass_id.customerFlag = true;

                        console.log("Success", closeTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  }
                  break;
                case "11":
                  let rfid = packet.slice(2, packet.length - 4);
                  rfid = await Utils.revHex(rfid);
                  rfid = parseInt(rfid, 16);
                  ack_res = socket.write(bufferRes);
                  console.log(`RFID in ${data}, ACK Out ${ack_str}, ${rfid}`);
                  if (ex_db_org_2b_bin_gs1 == 1 && ex_rfid_flag) {
                    ex_rfid_flag = false;
                    try {
                      const genTicket = await Utils.closeTicket({
                        rfid: `${rfid}`,
                      });
                      G_Exit_Pass_id.pass_id = rfid;
                      G_Exit_Pass_id.rfid_flag = true;
                      G_Exit_Pass_id.pass_id = false;
                      console.log("Success", genTicket.response);
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket closed ${JSON.stringify(genTicket)}`
                      );
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  break;
                case "82":
                  console.log(data);
                  let sac = data.slice(10, data.length - 4);
                  let deviceVersion = Utils.h2a(sac);
                  let deviceData = deviceVersion.split("V");
                  dataObj = {};
                  dataObj.siteId = siteId;
                  dataObj.deviceData = deviceVersion;

                  Utils.pushdata(
                    `${SiteDeviceVersionTopic}`,
                    JSON.stringify(dataObj)
                  );
                  console.log("Data for Device Version", deviceData[0]);
                  break;
                default:
                  console.log("Invalid Command !");
              }
            } else {
              console.log("Invalid Source !");
              return false;
            }
          }
        }
      } catch (err) {
        console.log("Error ", err);
      }
    } else if (onlineData.rows[0].is_online == false) {
      try {
        //

        //update device status to online

        const query11 = `UPDATE devices

      SET

      is_online = true

      WHERE terminal_ip_address='${socket.remoteAddress}'

    `;

        await client.query(query11);
        //Convert to HEX
        const data = Buffer.from(rawData).toString("hex");
        //
        console.log(data);

        if (data.length == 2) {
          console.log("TCP Client Open Barrier Flag", data);
          if (data == "01") {
            o_ent_barrier = true;
            // cf_flag = true;
          } else if (data == "11") {
            o_ext_barrier = true;
          }
        } else {
          const stx = data.slice(0, 2);
          const seq = data.slice(2, 4);
          const state = data.slice(4, 6);
          const command = data.slice(6, 8);
          console.log(command);

          const dataLen = parseInt(data.slice(8, 10), 16);
          const packet = data.slice(10, 2 * dataLen + 10);
          const xor = data.slice(10 + 2 * dataLen, 10 + 2 * dataLen + 2);
          const checksum = data.slice(
            10 + 2 * dataLen + 2,
            10 + 2 * dataLen + 4
          );

          if (command != 33) {
            console.log(
              `Input ------ (data -> ${data}, stx -> ${stx}, seq -> ${seq}, state ${state}, command -> ${command} dataLen ${dataLen}, packet -> ${packet}, xor -> ${xor}, checksum -> ${checksum})`
            );

            //Prepare ACK CMD
            // adding 0x80 = 128 decimal to command by using decimal value
            const ack_cmd_type = (parseInt(command, 16) + 128).toString(16),
              ack_data_len = "00";
            let ack_str = `${stx}${seq}${state}${ack_cmd_type}${ack_data_len}`,
              ack_res = "";
            //Get Xor and Checksum
            const ack_xor = Utils.computeXor(ack_str);
            const ack_checksum = Utils.computeChecksum(ack_str);
            ack_str = `${ack_str}${ack_xor}${ack_checksum}`;
            //Convert to Buffer from string
            const bufferRes = Buffer.from(ack_str, "hex");

            //Check if data is coming form entry or exit gate
            //Entry gate
            if (socket.remoteAddress == terminalData1.entryterminalData) {
              console.log("---Entry Gate data---");
              // Save raw data log
              try {
                const raw_log = await Utils.saveRawData({
                  gate: 1,
                  data: data,
                });
                console.log("Success", raw_log.response);
              } catch (err) {
                console.log("Error", err.response);
                Utils.logPublisher(1, 2, 1, "Error ${err.response}");
              }
              switch (command) {
                case "43":
                  const db_org = packet.slice(0, 8);
                  const cb_org = packet.slice(8, 16);
                  console.log("Data Bits", db_org);
                  console.log("Change Bits", cb_org);
                  const db_org_1b = db_org.slice(0, 2);
                  const db_org_2b = db_org.slice(2, 4);

                  const cb_org_1b = cb_org.slice(0, 2);
                  const cb_org_2b = cb_org.slice(2, 4);

                  const db_org_1b_bin = Utils.hex2bin(db_org_1b);
                  const db_org_2b_bin = Utils.hex2bin(db_org_2b);
                  const cb_org_1b_bin = Utils.hex2bin(cb_org_1b);
                  const cb_org_2b_bin = Utils.hex2bin(cb_org_2b);

                  const db_org_1b_bin_gs2 = db_org_1b_bin.charAt(1);
                  db_org_2b_bin_gs1 = db_org_2b_bin.charAt(4);
                  const db_org_2b_bin_cf = db_org_2b_bin.charAt(7);

                  console.log(
                    "Data Bits - GS1, GS2, CF",
                    db_org_2b_bin_gs1,
                    db_org_1b_bin_gs2,
                    db_org_2b_bin_cf
                  );

                  cb_org_1b_bin_gs2 = cb_org_1b_bin.charAt(1);
                  const cb_org_2b_bin_gs1 = cb_org_2b_bin.charAt(4);
                  cb_org_2b_bin_cf = cb_org_2b_bin.charAt(7);

                  console.log(
                    "Change Bits - GS1, GS2, CF",
                    cb_org_2b_bin_gs1,
                    cb_org_1b_bin_gs2,
                    cb_org_2b_bin_cf
                  );
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  //Get change bit
                  let change_bit = packet.slice(10, 12);
                  change_bit = Utils.hex2bin(change_bit);
                  //Card fetch
                  const card_fetch = change_bit.charAt(7);
                  //Ground sense
                  const ground_sense = change_bit.charAt(4);
                  //Ground sense 2
                  let gs = packet.slice(2, 4);
                  gs = Utils.hex2bin(gs);
                  gs = gs.charAt(4);
                  //Ground sense true
                  if (ground_sense == 1) {
                    ent_gs = true;
                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                    console.log("___GS1 Occurring__");
                    Utils.logPublisher(1, 1, 1, `CAR Detected`);
                    //Send Command to Scan/Press Button
                    try {
                      const sendCommand = await Utils.sendCommands({
                        gate: "in",
                        cmd_type: "33",
                      });
                      console.log("Success", sendCommand.response);
                    } catch (err) {
                      console.log("Error", err.response);
                    }
                  } else {
                    ent_gs = false;
                  }
                  console.log(
                    "========== 1",
                    db_org_2b_bin_gs1,
                    cb_org_2b_bin_cf,
                    cf_flag
                  );
                  //Card Fetch - true
                  if (card_fetch == 1) {
                    if (
                      db_org_2b_bin_gs1 == 1 &&
                      cb_org_2b_bin_cf == 1 &&
                      cf_flag
                    ) {
                      console.log("Card Fetch pressed");
                      Utils.logPublisher(1, 1, 1, `Card Fetch pressed`);
                      cf_flag = false;
                      console.log(
                        "========== 2",
                        db_org_2b_bin_gs1,
                        cb_org_2b_bin_cf,
                        cf_flag
                      );
                      //Create regular parking
                      try {
                        let genTicket = await Utils.genRegularParking();
                        G_Pass_id.pass_id = genTicket.pass_id;
                        G_Pass_id.passFlag = true;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = false;
                        console.log("Success", genTicket.status);
                        console.log("here");
                      } catch (err) {
                        console.log("Error", err.response);
                      }
                    } else {
                      console.log("Duplicate Card Fetch pressed");
                    }
                  }
                  //Ground Sense 2 is occurring
                  // if (cb_org_1b_bin_gs2 == 1 || db_org_2b_bin_gs1 == 0) {

                  if (
                    cb_org_1b_bin_gs2 == 1 ||
                    (db_org_2b_bin_gs1 == 0 && cb_org_2b_bin_gs1 == 1)
                  ) {
                    console.log("___Clearing the Screen__");
                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                    cf_flag = true;
                    rfid_flag = true;
                    try {
                      await Utils.sendCommands({
                        cmd_type: "31",
                        gate: "in",
                      });
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  if (cb_org_1b_bin_gs2 == 1) {
                    Utils.logPublisher(1, 1, 1, `CAR Entered`);
                    await Utils.updateTicket({ G_Pass_id });
                    G_Pass_id.passFlag = false;
                    G_Pass_id.pass_id = 0;
                    G_Pass_id.rfid_flag = false;
                    G_Pass_id.customerFlag = false;

                    if (g_carCount > 0 && g_carCount <= 100) {
                      g_carCount = g_carCount - 1;
                    }
                    console.log("___car passed____CarCount : ", g_carCount);
                    //Get data from sites
                    const query = "select * from sites";
                    const test = await client.query(query);

                    //Check the slot details
                    let data = {
                      no_of_slots: test.rows[0].no_of_slots,
                      available_slots: test.rows[0].available_slots,
                      occupied_slots: test.rows[0].occupied_slots,
                    };
                    console.log(data);

                    //Validate available slot is null or not
                    if (data.available_slots == 0 || null) {
                      data.available_slots = data.no_of_slots;
                    } else {
                      data.available_slots = data.available_slots - 1;
                    }

                    //Validate Occupied slot is null or not
                    if (data.occupied_slots == 0 || null) {
                      data.occupied_slots = 0;
                    } else {
                      data.occupied_slots = data.occupied_slots + 1;
                    }

                    console.log(
                      "Data",
                      data.available_slots,
                      data.occupied_slots
                    );

                    //Update site with the slot details
                    const query1 = `UPDATE SITES SET available_slots=${data.available_slots}, occupied_slots=${data.occupied_slots} where id=912`;
                    await client.query(query1);

                    console.log(
                      "================================== Entry Gate ======================================================\n"
                    );
                  }
                  console.log(`State Change in ${data}, ACK Out ${ack_str}`);
                  break;

                case "33":
                  ack_res = socket.write(bufferRes);
                  console.log(`HeartBeat in ${data}, ACK Out ${ack_str}`);
                  break;
                case "1a":
                  console.log("GS1, CF", db_org_2b_bin_gs1, cf_flag);
                  if (db_org_2b_bin_gs1 == 1 && cf_flag) {
                    const tempData = Utils.h2a(
                      packet.slice(2, packet.length - 4)
                    );
                    if (tempData.includes("https")) {
                      const passArr = tempData.split("/");
                      const passId = passArr[passArr.length - 1];
                      Utils.logPublisher(1, 1, 1, `Reservation Ticket scanned`);
                      console.log(
                        "Open Ticket for Pass ID",
                        passArr[passArr.length - 1]
                      );
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const genTicket = await Utils.genTicket({
                          pass_id: passId,
                        });
                        G_Pass_id.pass_id = passId;
                        G_Pass_id.passFlag = true;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = false;

                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    } else {
                      const tempData = Utils.h2a(
                        packet.slice(0, packet.length - 4)
                      );
                      const customerId = tempData;
                      Utils.logPublisher(
                        1,
                        1,
                        1,
                        `Customer QR code scanned ${customerId}`
                      );
                      console.log(customerId, "++++");
                      console.log("Open Ticket for Customer ID", customerId);
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const genTicket = await Utils.genTicket({
                          customer_id: customerId,
                        });
                        console.log(genTicket.msg);
                        G_Pass_id.pass_id = genTicket.msg;
                        G_Pass_id.passFlag = false;
                        G_Pass_id.rfid_flag = false;
                        G_Pass_id.customerFlag = true;

                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  }
                  break;
                case "11":
                  let rfid = packet.slice(2, packet.length - 4);
                  rfid = await Utils.revHex(rfid);
                  rfid = parseInt(rfid, 16);
                  ack_res = socket.write(bufferRes);
                  console.log(`RFID in ${data}, ACK Out ${ack_str}, ${rfid}`);
                  Utils.logPublisher(1, 1, 1, `RFID Scanned ${data}`);

                  if (db_org_2b_bin_gs1 == 1 && rfid_flag) {
                    rfid_flag = false;
                    console.log("Duplicate RFID");
                    try {
                      const genTicket = await Utils.genTicket({
                        rfid: `${rfid}`,
                      });
                      G_Pass_id.pass_id = rfid;
                      G_Pass_id.passFlag = false;
                      G_Pass_id.rfid_flag = true;
                      console.log("Success", genTicket.response);
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  break;
                case "82":
                  console.log(data);
                  let sac = data.slice(10, data.length - 4);
                  let deviceVersion = Utils.h2a(sac);
                  let deviceData = deviceVersion.split("V");
                  dataObj = {};
                  dataObj.siteId = siteId;
                  dataObj.deviceData = deviceVersion;

                  Utils.pushdata(
                    `${SiteDeviceVersionTopic}`,
                    JSON.stringify(dataObj)
                  );

                  console.log("Data for Device Version", deviceData[0]);
                  break;
                default:
                  console.log("Invalid Command !");
              }
            }
            //Exit gate
            else if (socket.remoteAddress == terminalData1.exitterminalData) {
              //Exit Gate
              console.log("---Exit Gate data---");
              //Save raw data log
              try {
                const raw_log = await Utils.saveRawData({
                  gate: 2,
                  data: data,
                });
                console.log("Success", raw_log.response);
              } catch (err) {
                console.log("Error", err.response);
              }
              switch (command) {
                case "44":
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  console.log(`NEW in ${data}, ACK Out ${ack_str}`);

                  break;
                case "43":
                  const db_org = packet.slice(0, 8);
                  const cb_org = packet.slice(8, 16);
                  console.log("Exit Data Bits", db_org);
                  console.log("Exit Change Bits", cb_org);
                  const db_org_1b = db_org.slice(0, 2);
                  const db_org_2b = db_org.slice(2, 4);

                  const cb_org_1b = cb_org.slice(0, 2);
                  const cb_org_2b = cb_org.slice(2, 4);

                  const db_org_1b_bin = Utils.hex2bin(db_org_1b);
                  const db_org_2b_bin = Utils.hex2bin(db_org_2b);
                  const cb_org_1b_bin = Utils.hex2bin(cb_org_1b);
                  const cb_org_2b_bin = Utils.hex2bin(cb_org_2b);

                  const db_org_1b_bin_gs2 = db_org_1b_bin.charAt(1);
                  ex_db_org_2b_bin_gs1 = db_org_2b_bin.charAt(4);
                  const db_org_2b_bin_cf = db_org_2b_bin.charAt(7);

                  console.log(
                    "Data Bits - GS1, GS2, CF",
                    ex_db_org_2b_bin_gs1,
                    db_org_1b_bin_gs2,
                    db_org_2b_bin_cf
                  );

                  ex_cb_org_1b_bin_gs2 = cb_org_1b_bin.charAt(1);
                  const cb_org_2b_bin_gs1 = cb_org_2b_bin.charAt(4);
                  ext_cb_org_2b_bin_cf = cb_org_2b_bin.charAt(7);

                  console.log(
                    "Change Bits - GS1, GS2, CF",
                    cb_org_2b_bin_gs1,
                    ex_cb_org_1b_bin_gs2,
                    ext_cb_org_2b_bin_cf
                  );
                  // write data to device
                  ack_res = socket.write(bufferRes);
                  //Get change bit
                  let change_bit = packet.slice(10, 12);
                  change_bit = Utils.hex2bin(change_bit);

                  let ground_sence = change_bit.charAt(4);
                  //Ground sense true
                  if (ground_sence == 1) {
                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                    console.log("Ground sense true");
                    Utils.logPublisher(1, 1, 2, `CAR Detected`);
                    //Create regular parking
                    try {
                      const sendCommand = await Utils.sendCommands({
                        gate: "out",
                        cmd_type: "34",
                      });
                      console.log("Success", sendCommand.response);
                    } catch (err) {
                      console.log("Error", err.response);
                    }
                  }
                  if (ex_cb_org_1b_bin_gs2 == 1) {
                    ex_rfid_flag = true;
                    console.log("________GS2 Occurring_______");
                    Utils.logPublisher(1, 1, 2, `CAR EXIT`);
                    try {
                      // await Utils.sendCommands({
                      //   cmd_type: "11",
                      //   gate: "in",
                      // });
                      // await Utils.sendCommands({
                      //   cmd_type: "31",
                      //   gate: "out",
                      // });
                      await Utils.exitUpdateTicket({
                        G_Exit_Pass_id,
                      });

                      G_Exit_Pass_id.pass_id = 0;
                      G_Exit_Pass_id.rfid_flag = false;
                      G_Exit_Pass_id.passFlag = false;
                      G_Exit_Pass_id.customerFlag = false;
                      if (g_carCount > 0 && g_carCount != 100) {
                        g_carCount = g_carCount + 1;
                      }
                      console.log("CarCount : ", g_carCount);

                      //Get data from sites
                      const query = "select * from sites";
                      const test = await client.query(query);

                      //Get Slot details
                      let data = {
                        no_of_slots: test.rows[0].no_of_slots,
                        available_slots: test.rows[0].available_slots,
                        occupied_slots: test.rows[0].occupied_slots,
                      };
                      console.log(data);

                      //Check available slot is there or not
                      if (data.available_slots == 0 || null) {
                        data.available_slots = data.no_of_slots;
                      } else {
                        data.available_slots = data.available_slots + 1;
                      }

                      //Check occupied slot is there or not
                      if (data.occupied_slots == 0 || null) {
                        data.occupied_slots = 0;
                      } else {
                        data.occupied_slots = data.occupied_slots - 1;
                      }

                      console.log(
                        "Data",
                        data.available_slots,
                        data.occupied_slots
                      );

                      //Update the site with slot details
                      const query1 = `UPDATE SITES SET available_slots=${data.available_slots}, occupied_slots=${data.occupied_slots} where id=912`;
                      await client.query(query1);
                    } catch (err) {
                      console.log("Error", err);
                    }

                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                  }
                  //Ground Sense 2 is occurring
                  if (
                    ex_cb_org_1b_bin_gs2 == 1 ||
                    (ex_db_org_2b_bin_gs1 == 0 && cb_org_2b_bin_gs1 == 1)
                  ) {
                    ex_rfid_flag = true;
                    console.log(
                      "================================== Exit Gate ======================================================\n"
                    );
                    console.log("________Clearing the screen _______");
                    try {
                      // await Utils.sendCommands({
                      //   cmd_type: "11",
                      //   gate: "in",
                      // });
                      G_Exit_Pass_id.pass_id = 0;
                      G_Exit_Pass_id.rfid_flag = false;
                      G_Exit_Pass_id.passFlag = false;
                      G_Exit_Pass_id.customerFlag = false;
                      await Utils.sendCommands({
                        cmd_type: "31",
                        gate: "out",
                      });
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }

                  console.log(`State Change in ${data}, ACK Out ${ack_str} `);
                  break;
                case "33":
                  ack_res = socket.write(bufferRes);
                  console.log(`HeartBeat in ${data}, ACK Out ${ack_str}`);
                  break;
                case "19":
                  if (packet.slice(0, 2) == "01") {
                    let rfid = Utils.h2a(packet.slice(2, packet.length - 4));
                    rfid = rfid.slice(-5);
                    rfid = parseInt(rfid, 16);
                    ack_res = socket.write(bufferRes);
                    console.log(
                      `RFID Out ${data}, ACK Out ${ack_str}, ${rfid}`
                    );
                    if (ex_db_org_2b_bin_gs1 == 1 && ex_rfid_flag) {
                      ex_rfid_flag = false;
                      try {
                        const genTicket = await Utils.closeTicket({
                          rfid: `${rfid}`,
                        });
                        console.log("Success", genTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  } else if (packet.slice(0, 2) == "02") {
                    if (G_Exit_Pass_id.passFlag == true) {
                      const data = await Utils.posParser(packet);
                      console.log(data);
                      if (data.status == "000") {
                        try {
                          // const sendCommand = await Utils.sendCommands({
                          //   gate: "out",
                          //   cmd_type: "30",
                          //   text: "Payment Failed",
                          // });
                          console.log("Payment Successfull");

                          await Utils.capturePayment({
                            passId: G_Exit_Pass_id.pass_id,
                            status: 1,
                            recepit_no: data.recepit_no,
                            transaction_no: data.transaction_no,
                          });
                        } catch (err) {
                          console.log("Error", err.response);
                        }
                      } else {
                        try {
                          console.log("Payment Cancel");
                          Utils.logPublisher(1, 1, 2, `Payment Cancel`);
                          // const sendCommand = await Utils.sendCommands({
                          //   gate: "out",
                          //   cmd_type: "32",
                          // });
                          // await new Promise((resolve) =>
                          //   setTimeout(resolve, 500)
                          // );

                          console.log("PassId", G_Exit_Pass_id.pass_id);
                          await Utils.capturePayment({
                            passId: G_Exit_Pass_id.pass_id,
                            status: 0,
                          });
                          // await Utils.sendCommands({
                          //   cmd_type: "10",
                          //   gate: "out",
                          // });

                          console.log("Success", sendCommand.response);
                        } catch (err) {
                          console.log("Error", err.response);
                        }
                      }
                    }
                  }

                  break;
                case "1a":
                  if (ex_db_org_2b_bin_gs1 == 1) {
                    const tempData = Utils.h2a(
                      packet.slice(2, packet.length - 4)
                    );

                    if (tempData.includes("https")) {
                      const passArr = tempData.split("/");
                      const passId = passArr[passArr.length - 1];
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket scanned for passId: ${passId}`
                      );
                      console.log(
                        "Close Ticket for Pass ID",
                        passArr[passArr.length - 1]
                      );
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const closeTicket = await Utils.closeTicket({
                          pass_id: passId,
                        });
                        G_Exit_Pass_id.pass_id = passId;
                        G_Exit_Pass_id.rfid_flag = false;
                        G_Exit_Pass_id.passFlag = true;
                        G_Exit_Pass_id.customerFlag = false;
                        console.log("Success", closeTicket.response);
                      } catch (err) {
                        console.log("Error", err.response);
                      }
                    } else {
                      const tempData = Utils.h2a(
                        packet.slice(0, packet.length - 4)
                      );
                      const customerId = tempData;
                      console.log(customerId, "++++");
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket scanned for customerId: ${customerId}`
                      );
                      console.log("Close Ticket for Customer ID", customerId);
                      ack_res = socket.write(bufferRes);
                      console.log(`QR in ${data}, ACK Out ${ack_str}`);
                      try {
                        const closeTicket = await Utils.closeTicket({
                          customer_id: customerId,
                        });
                        console.log("closeTicket", closeTicket.id);
                        G_Exit_Pass_id.pass_id = closeTicket.id;
                        G_Exit_Pass_id.passFlag = false;
                        G_Exit_Pass_id.rfid_flag = false;
                        G_Exit_Pass_id.customerFlag = true;

                        console.log("Success", closeTicket.response);
                      } catch (err) {
                        console.log("Error", err);
                      }
                    }
                  }
                  break;
                case "11":
                  let rfid = packet.slice(2, packet.length - 4);
                  rfid = await Utils.revHex(rfid);
                  rfid = parseInt(rfid, 16);
                  ack_res = socket.write(bufferRes);
                  console.log(`RFID in ${data}, ACK Out ${ack_str}, ${rfid}`);
                  if (ex_db_org_2b_bin_gs1 == 1 && ex_rfid_flag) {
                    ex_rfid_flag = false;
                    try {
                      const genTicket = await Utils.closeTicket({
                        rfid: `${rfid}`,
                      });
                      G_Exit_Pass_id.pass_id = rfid;
                      G_Exit_Pass_id.rfid_flag = true;
                      G_Exit_Pass_id.pass_id = false;
                      console.log("Success", genTicket.response);
                      Utils.logPublisher(
                        1,
                        1,
                        2,
                        `Ticket closed ${JSON.stringify(genTicket)}`
                      );
                    } catch (err) {
                      console.log("Error", err);
                    }
                  }
                  break;
                case "82":
                  console.log(data);
                  let sac = data.slice(10, data.length - 4);
                  let deviceVersion = Utils.h2a(sac);
                  let deviceData = deviceVersion.split("V");
                  dataObj = {};
                  dataObj.siteId = siteId;
                  dataObj.deviceData = deviceVersion;

                  Utils.pushdata(
                    `${SiteDeviceVersionTopic}`,
                    JSON.stringify(dataObj)
                  );
                  console.log("Data for Device Version", deviceData[0]);
                  break;
                default:
                  console.log("Invalid Command !");
              }
            } else {
              console.log("Invalid Source !");
              return false;
            }
          }
        }
      } catch (err) {
        console.log("Error ", err);
      }
    } else {
      console.log("invalid");
    }
  });
  // Add a 'close' event handler to this instance of socket
  socket.on("close", (data) => {
    let index = sockets.findIndex((o) => {
      return (
        o.remoteAddress === socket.remoteAddress &&
        o.remotePort === socket.remotePort
      );
    });
    if (index !== -1) sockets.splice(index, 1);
    sockets.forEach((sock) => {
      sock.write(`${clientAddress} disconnected\n`);
    });

    console.log(new Date(), `Connection closed: ${clientAddress}`);
    Utils.logPublisher(
      1,
      1,
      3,
      `${new Date()}, Connection closed: ${clientAddress}`
    );
  });
  // Add a 'error' event handler to this instance of socket
  socket.on("error", (err) => {
    console.log(err);
    console.log(`Error occurred in ${clientAddress}: ${err.message}`);
    Utils.logPublisher(
      1,
      2,
      3,
      `Error occurred in ${clientAddress}: ${err.message}`
    );
  });
  // If there is a Idle connection more thanm 60 sec, update db and reboot
  socket.setTimeout(60000, () => {
    console.log("Timeout called", socket.remoteAddress);
    if (socket.remoteAddress == terminalData1.entryterminalData) {
      try {
        //Update device to offline
        const query9 = `UPDATE devices
                    SET
                    is_online = false
                    WHERE terminal_ip_address='${socket.remoteAddress}'
                    `;
        client.query(query9);
        //Get Device Version
        const sendCommand1 = Utils.sendCommands({
          gate: "in",
          cmd_type: "88",
        });
        console.log("Success", sendCommand1.response);
      } catch (error) {
        console.log("Err", error);
      }
    } else if (socket.remoteAddress == terminalData1.exitterminalData) {
      try {
        //Update device to offline
        const query9 = `UPDATE devices
                    SET
                    is_online = false
                    WHERE terminal_ip_address='${socket.remoteAddress}'
                    `;
        client.query(query9);
        //Get Device Version
        const sendCommand1 = Utils.sendCommands({
          gate: "out",
          cmd_type: "88",
        });
        console.log("Success", sendCommand1.response);
      } catch (error) {
        console.log("Err", error);
      }
    } else {
      console.log("Invalid Address ++++++++++++++++++++++++++++");
      return false;
    }
  });
});
