//All Utils
class Utils {
  //Ascii to Hex
  asciiToHex(input) {
    return Buffer.from(input, 'ascii').toString('hex');
  }
  // converting hex to binary
  hex2bin(hex) {
    return ('00000000' + parseInt(hex, 16).toString(2)).substr(-8);
  }
  h2a(str) {
    const hex = str.toString();
    let result = '';
    for (var n = 0; n < hex.length; n += 2) {
      result += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return result;
  }
  //Hex to string // ASCII
  h22a(arr) {
    let parserTest = [];
    arr.forEach((element) => {
      const hex = element.toString();
      let result = '';
      for (let n = 0; n < hex.length; n += 2) {
        result += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
      }
      parserTest.push(result);
    });
    return parserTest;
  }
  //Compute Checksum
  computeChecksum(input) {
    let temp = '0000';
    for (let k = 0; k < input.length; k = k + 2) {
      const a = temp;
      const b = '00' + input.slice(k, k + 2);
      let ndigits = 4,
        i,
        carry = 0,
        d,
        result = '';

      for (i = ndigits - 1; i >= 0; i--) {
        d = parseInt(a[i], 16) + parseInt(b[i], 16) + carry;
        carry = d >> 4;
        result = (d & 15).toString(16) + result;
      }
      temp = result;
    }
    console.log(`Checksum of -> ${input} is ${temp.slice(-2)}`);
    return temp.slice(-2);
  }
  //Compute XOR
  computeXor(input) {
    let xor_temp = '00';
    let xor_str = xor_temp;
    for (let i = 0; i < input.length; i = i + 2) {
      const buf1 = Buffer.from(xor_str, 'hex');
      const buf2 = Buffer.from(input.slice(i, i + 2), 'hex');
      const bufResult = buf1.map((b, i) => b ^ buf2[i]);
      xor_str = bufResult.toString('hex');
    }
    console.log(`XOR of -> ${input} is ${xor_str}`);
    return xor_str;
  }
  //Compute POS Command for purchase
  computePosCmd(data) {
    function toASCII(str) {
      let result = '';
      for (let i = 0; i < str.length; i++) {
        result += str.charCodeAt(i).toString(16);
      }
      return result;
    }
    let d = data.Amount.split(' ');
    let b = d[0] * 100;
    let fs = '1c';
    if (data.MSGID == 'PUR') {
      const cmd =
        toASCII(data.MSGID) +
        toASCII(data.ECRNO) +
        fs +
        toASCII(data.RecieptNo) +
        fs +
        toASCII(b.toString().padStart(12, 0)) +
        fs +
        fs +
        fs +
        fs +
        fs;
      const xor = this.computeXor(cmd);
      return `02${cmd}${xor}`;
    }
  }

  async posParser(input) {
    const data = input.split('1c');
    const temp = [];
    temp.push(data[1], data[3], data[11]);
    const parserTest = this.h22a(temp);
    const posStatus = {
      status: parserTest[0],
      recepit_no: parserTest[1],
      transaction_no: parserTest[2],
    };
    return posStatus;
  }

  //Call Exit ticket manager
  async closeTicket(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: 'POST',
        url: `http://localhost:9001/exit-ticket-manager`,
        headers: {},
        data,
      });
      console.log('2=====> Response from Exit Ticket Manager', response.data);
      return true;
    } catch (err) {
      console.log('3 =====> Error!', err);
      return false;
    }
  }

  //capture payment
  async capturePayment(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: 'POST',
        url: `http://localhost:9001/payments`,
        headers: {},
        data,
      });
      console.log('2=====> Response from payments', response.data);
      return true;
    } catch (err) {
      console.log('3 =====> Error!', err);
      return false;
    }
  }
}
module.exports = Utils;
