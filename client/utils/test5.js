const data = '7FAD0043084009000000080000D8C8';
//7F0A00430840090000000800007F25
//7FOE003301014202
const stx = data.slice(0, 2);
const seq = data.slice(2, 4);
const state = data.slice(4, 6);
const command = data.slice(6, 8);
const dataLen = parseInt(data.slice(8, 10), 16);
const packet = data.slice(10, 2 * dataLen + 10);
const xor = data.slice(10 + 2 * dataLen, 10 + 2 * dataLen + 2);
const checksum = data.slice(10 + 2 * dataLen + 2, 10 + 2 * dataLen + 4);
console.log(
  ` 'Print ------>' data -> ${data}, stx -> ${stx}, seq -> ${seq}, state ${state}, command -> ${command} dataLen ${dataLen}, packet -> ${packet}, xor -> ${xor}, checksum -> ${checksum}`
);

function computeXor(input) {
  let xor_temp = '00';
  let xor_str = xor_temp;
  for (let i = 0; i < input.length; i = i + 2) {
    const buf1 = Buffer.from(xor_str, 'hex');
    const buf2 = Buffer.from(input.slice(i, i + 2), 'hex');
    const bufResult = buf1.map((b, i) => b ^ buf2[i]);
    xor_str = bufResult.toString('hex');
  }
  return xor_str;
}

console.log(computeXor('7F0B0043084008000000010000'));
