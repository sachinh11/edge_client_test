/*jshint esversion: 8 */
const path = require("path");
//Get all the settings
const setting = require("./settings");

module.exports = {
  appName: "SP Ticket Manager",
  env: setting.appEnv,
  port: setting.appPort,
  logs: setting.appLog,
  //ip:'localhost', //This will not let app accessible on LAN
  ip: setting.appHost,

  root: path.normalize(`${__dirname}/../..`), // root
  base: path.normalize(`${__dirname}/..`), // base
  mqttUrl: setting.mqttUrl,
  mqttTopic: setting.mqttTopic,

  logFileName: {
    info: "info.log",
    error: "exceptions.log",
  },
  spdb: {
    pg: {
      // PGSQL - Sample URI
      // uri: 'postgres://user:pass@example.com:5432/dbname'
      uri: (() => {
        //If Username Password is set
        if (setting.pgdbIsAuth === "true") {
          return `postgres://${setting.pgdbUsername}:${setting.pgdbPassword}@${setting.pgdbHost}:${setting.pgdbPort}/${setting.pgDbName}`;
        }
        //Without auth
        return `postgres://${setting.pgdbHost}:${setting.pgdbPort}/${setting.pgDbName}`;
      })(),

      masterDb: `${setting.pgDbName}`,
      options: {},
      host: setting.pgdbHost,
      port: setting.pgdbPort,
      username: setting.pgdbUsername,
      password: setting.pgdbPassword,
    },
  },
  edgeController: {
    entryIP: setting.entryControllerIP,
    exitIP: setting.exitControllerIP,
    entryPort: setting.entryControllerPort,
    exitPort: setting.exitControllerPort,
    spPayIP: setting.spPayControllerIP,
    spPayPort: setting.spPayControllerPort,
  },
  minio: {
    url: setting.url,
    accessKey: setting.accessKey,
    secretKey: setting.secretKey,
    port: setting.port,
    bucket_name: setting.bucket_name,
    publicUrl: setting.publicUrl,
  },
};
