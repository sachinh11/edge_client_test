//Entry Ticket Manager
(async (ctx, next) => {
  //Get request body
  const input = ctx.request.body;
  console.log(input);
  //if pass_id present
  if (input && input.pass_id) {
    //Call Ticket generation API
    // fetch tickets/reservation
    const query1 = `select * from reservations where pass_id='${input.pass_id}'`;
    const res = await client.query(query1);
    const data1 = res.rows[0];

    //validate reservation is present or not
    if (res.rows.length > 0) {
      //Check number plate from anpr is already used or not
      if (g_entry_object.entry_flag == false) {
        //Check reservation status
        if (data1.status_id == 5) {
          console.log("Ticket Already generated, In progress !", input.pass_id);
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Ticket Already generated, In progress !",
          };
          return ctx.body;
        } else if (data1.status_id == 2) {
          console.log("Ticket Already generated, Completed !", input.pass_id);
          return "Ticket Already generated, Completed !";
        } else if (data1.status_id == 1) {
          console.log("Reservation found for passId !", input.pass_id);
          let entry_time = new Date(data1.start_time).getTime();
          let current_entry_time = new Date().getTime();
          let exit_time = new Date(data1.end_time).getTime();
          //cal diff between entry and exit time
          const timeDiff = current_entry_time - entry_time;
          // get sec
          let sec = timeDiff / 1000;
          console.log(sec);
          console.log(current_entry_time, exit_time);
          if (current_entry_time < exit_time) {
            //Customer late from the given time
            if (sec > 300) {
              console.log("Cutomer entry is late");
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1 for late entry");
                  const entry_time_ticket = moment(new Date()).format();
                  const exit_time_ticket = moment(data1.end_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id, exit_time)
                    VALUES ('${data1.id}', '${data1.site_id}',cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                  console.log("Data", query2);
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Paymnet was not done
              else {
                try {
                  console.log("Not paid for late entry");
                  const entry_time_ticket = moment(data1.start_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id)
                      VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // Cutomer early to the given time
            else if (sec > -10800 && sec < 0) {
              console.log("cutsomer entry is early");
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1 for early entry");
                  const entry_time_ticket = moment(data1.start_time).format();

                  const exit_time_ticket = moment(data1.end_time).format();
                  //cal diff between entry and exit time
                  const timeDiff = entry_time_ticket - exit_time_ticket;
                  // get hours
                  let hh =
                    Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                      ? Math.ceil(timeDiff / 1000 / 60 / 60)
                      : 1;

                  let ex_time = moment(new Date()).add(hh, "hours").format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), now(), '${data1.pass_id}', '${ex_time}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Cutomer not made the paymnet
              else {
                try {
                  console.log("Not paid for late entry");
                  const entry_time_ticket = moment(new Date()).format();

                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // cutomer reached within the grace period
            else if (sec > 0 && sec < 300) {
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1 within the time");
                  const entry_time_ticket = moment(data1.start_time).format();
                  const exit_time_ticket = moment(data1.end_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id,number_plate,entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Cutomer not made the paymnet
              else {
                try {
                  console.log("Not paid, within the time");
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id, exit_time)
                VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), now(), '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  g_entry_object.entry_flag = true;
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // Invalid ticket
            else {
              console.log("Enterd site way to early!", input.pass_id);

              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);

              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Enterd site way to early!",
              };
              return ctx.body;
            }
          } else {
            console.log("Invalid Ticket", input.pass_id);

            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 5000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','33')`;
            await client.query(query4);

            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid Ticket",
            };
            return ctx.body;
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          return "Invalid reservation status !";
        }
      } else {
        console.log("entry object falg is false");
        //Check reservation status
        if (data1.status_id == 5) {
          console.log("Ticket Already generated, In progress !", input.pass_id);
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Ticket Already generated, In progress !",
          };
          return ctx.body;
        } else if (data1.status_id == 2) {
          console.log("Ticket Already generated, Completed !", input.pass_id);
          return "Ticket Already generated, Completed !";
        } else if (data1.status_id == 1) {
          console.log("Reservation found for passId !", input.pass_id);
          let entry_time = new Date(data1.start_time).getTime();
          let current_entry_time = new Date().getTime();
          //cal diff between entry and exit time
          const timeDiff = current_entry_time - entry_time;
          // get sec
          let sec = timeDiff / 1000;
          console.log(sec);
          if (current_entry_time < exit_time) {
            //Customer late from the given time
            if (sec > 300) {
              // get hours
              let hh =
                Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                  ? Math.ceil(timeDiff / 1000 / 60 / 60)
                  : 1;
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1 for late entry");
                  const entry_time_ticket = moment(new Date()).format();
                  const exit_time_ticket = moment(data1.end_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
                    VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Paymnet was not done
              else {
                try {
                  console.log("Not paid for late entry");
                  const entry_time_ticket = moment(data1.start_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id)
                      VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // Cutomer early to the given time
            else if (sec > -10800 && sec < 0) {
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1 for late entry");
                  const entry_time_ticket = moment(data1.start_time).format();

                  const exit_time_ticket = moment(data1.end_time).format();
                  //cal diff between entry and exit time
                  const timeDiff = entry_time_ticket - exit_time_ticket;
                  // get hours
                  let hh =
                    Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                      ? Math.ceil(timeDiff / 1000 / 60 / 60)
                      : 1;

                  let ex_time = moment(new Date()).add(hh, "hours").format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', now(), '${data1.pass_id}', '${ex_time}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Cutomer not made the paymnet
              else {
                try {
                  console.log("Not paid for late entry");
                  const entry_time_ticket = moment(new Date()).format();

                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // cutomer reached within the grace period
            else if (sec > 0 && sec < 300) {
              console.log("Entry with the time");
              //Cutomer alreday made the paymnet
              if (data1.payment_status_id == 1) {
                try {
                  console.log("Payment status id is 1");
                  const entry_time_ticket = moment(data1.start_time).format();
                  const exit_time_ticket = moment(data1.end_time).format();
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
              //Cutomer not made the paymnet
              else {
                try {
                  console.log("Not paid, within the time");
                  //Insert ticket
                  let query2 = `INSERT INTO tickets (reservation_id, site_id,entry_time, pass_id, exit_time)
                VALUES ('${data1.id}', '${data1.site_id}', now(), '${data1.pass_id}')`;
                  await client.query(query2);
                  console.log("Step 1");
                  //Insert into commands to open barrier
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                  await client.query(query3);
                  console.log("Step 2");
                  //Insert commands to close barrier after 1 minute
                  // await new Promise((resolve) => setTimeout(resolve, 2000));
                  //Insert into commands
                  let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                  await client.query(query4);

                  //Update Reservation Status
                  let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                  await client.query(query5);
                  console.log("Step 4");
                  //Return
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "Ticket has been generated!",
                  };
                  return ctx.body;
                } catch (err) {
                  console.log(err);
                  console.log("Unable to create ticket!");
                }
              }
            }
            // Invalid ticket
            else {
              console.log("Enterd site way to early!", input.pass_id);

              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);

              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Enterd site way to early!",
              };
              return ctx.body;
            }
          } else {
            console.log("Invalid Ticket", input.pass_id);

            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 5000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','33')`;
            await client.query(query4);

            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid Ticket",
            };
            return ctx.body;
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          return "Invalid reservation status !";
        }
      }
    } else {
      console.log("No Reservation for passId !", input.pass_id);
      try {
        //Invalid ticket command
        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','35')`;
        await client.query(query3);

        await new Promise((resolve) => setTimeout(resolve, 3000));

        //Press or Scan Ticket
        let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','33')`;
        await client.query(query4);
      } catch (err) {
        console.log(err);
      }
    }
  }
  //if rfid present
  else if (input && input.rfid) {
    console.log("----------RFID Scan", input);
    try {
      if (g_entry_object.entry_flag == false) {
        // fetch tickets/reservation
        const query1 = `select * from smart_tags where uid='${input.rfid}'`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (data1) {
          try {
            //Insert rfid ticket
            let query2 = `INSERT INTO rfid_tickets(rfid, status_id, entry_time, number_plate)
              VALUES ('${data1.uid}', 3, now(), '${g_entry_object.number_plate}')`;
            await client.query(query2);

            //Insert into commands to open barrier
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
            await client.query(query3);

            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Valid RFID!",
            };
            return ctx.body;
          } catch (err) {
            console.log(err);
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Error !",
            };
          }
        } else {
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "RFID not registered!",
          };
          return ctx.body;
        }
      } else {
        // fetch tickets/reservation
        const query1 = `select * from smart_tags where uid='${input.rfid}'`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (data1) {
          try {
            //Insert rfid ticket
            let query2 = `INSERT INTO rfid_tickets(rfid, status_id, entry_time)
              VALUES ('${data1.uid}', 1, now())`;
            await client.query(query2);

            //Insert into commands to open barrier
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
            await client.query(query3);

            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Valid RFID!",
            };
            return ctx.body;
          } catch (err) {
            console.log(err);
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Error !",
            };
          }
        } else {
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "RFID not registered!",
          };
          return ctx.body;
        }
      }
    } catch (err) {
      console.log(err);
      //Return
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Error !",
      };
    }
  }
  //if customer_id present
  else if (input && input.customer_id) {
    // fetch tickets/reservation
    const query50 = `select * from customers where customer_id='${input.customer_id}'`;
    const resp = await client.query(query50);
    const cid = resp.rows[0];

    //validate cutomer is present or not
    if (resp.rows.length > 0) {
      try {
        //Generate passId with current time
        const passId = new Date().valueOf();
        const data = {
          vehicle_id: input.vehicle_id || null,
          site_parking_slot_id: config.site_parking_slot_id,
          reservation_type_id: config.reservation_type_id,
          end_time: null,
          site_id: config.site_id,
          status_id: config.status_id,
          pass_id: passId.toString(),
        };

        //Check number plate from anpr is already used or not
        if (g_entry_object.entry_flag == false) {
          //Insert reservation
          let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount, number_plate, customer_id) 
                  VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100, '${g_entry_object.number_plate}', '${cid.customer_id}')`;
          await client.query(query);
          //Get Reservation Details
          const query2 = `select * from reservations where pass_id='${data.pass_id}'`;
          const res1 = await client.query(query2);
          const data2 = res1.rows[0];
          if (res.rows.length > 0) {
            try {
              //Insert ticket
              let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, number_plate,entry_time, pass_id, status_id, customer_id)
                    VALUES ('${data2.id}', '${data2.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), '${g_entry_object.number_plate}', now(),'${data2.pass_id}', 4, '${cid.customer_id}')`;
              await client.query(query2);
              let cmd = {
                url: `https://demo.website.sp.hyperthings.in/ticket/${data1.pass_id}`,
                pass_id: data1.pass_id,
              };
              cmd = JSON.stringify(cmd);

              //Insert Command to print tickets
              let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
              await client.query(query6);
              await new Promise((resolve) => setTimeout(resolve, 500));
              //Insert into commands to open barrier
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
              await client.query(query3);
              //Insert commands to close barrier after 1 minute
              // setTimeout(async () => {
              //   //Insert into commands
              //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
              //   await client.query(query4);
              // }, 3000);
              //Update Reservation Status
              let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
              await client.query(query5);
              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                // msg: `Ticket and Command has been generated - ${data.pass_id}`,
                msg: `${data.pass_id}`,
              };
              g_entry_object.entry_flag = true;
              return ctx.body;
            } catch (err) {
              console.log(err);
              console.log("Unable to create ticket!");
              ctx.status = 404;
              ctx.body = {
                success: false,
                msg: "Unable to create ticket!",
              };
              return ctx.body;
            }
          } else {
            console.log("No Reservation found for passId !", data.pass_id);
          }
        } else {
          //Insert reservation
          let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount, customer_id) 
                  VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100, '${cid.customer_id}')`;

          await client.query(query);
          //Get Reservation Details
          const query2 = `select * from reservations where pass_id='${data.pass_id}'`;
          const res1 = await client.query(query2);
          const data2 = res1.rows[0];
          if (res.rows.length > 0) {
            try {
              //Insert ticket
              let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, entry_time, pass_id, status_id, customer_id)
                    VALUES ('${data2.id}', '${data2.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), now(),'${data2.pass_id}', 4, '${cid.customer_id}')`;
              await client.query(query2);
              let cmd = {
                url: `https://demo.website.sp.hyperthings.in/ticket/${data2.pass_id}`,
                pass_id: data2.pass_id,
              };
              cmd = JSON.stringify(cmd);

              //Insert Command to print tickets
              let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
              await client.query(query6);
              await new Promise((resolve) => setTimeout(resolve, 500));
              //Insert into commands to open barrier
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
              await client.query(query3);
              //Insert commands to close barrier after 1 minute
              // setTimeout(async () => {
              //   //Insert into commands
              //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
              //   await client.query(query4);
              // }, 3000);
              //Update Reservation Status
              let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
              await client.query(query5);
              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                // msg: `Ticket and Command has been generated - ${data.pass_id}`,
                msg: `${data.pass_id}`,
              };
              g_entry_object.entry_flag = true;
              return ctx.body;
            } catch (err) {
              console.log(err);
              console.log("Unable to create ticket!");
              ctx.status = 404;
              ctx.body = {
                success: false,
                msg: "Unable to create ticket!",
              };
              return ctx.body;
            }
          } else {
            console.log("No Reservation found for passId !", data.pass_id);
          }
        }
      } catch (error) {
        console.log(error);
        console.log("Unable to create reservation!");
        ctx.status = 404;
        ctx.body = {
          success: false,
          msg: "Unable to create reservation!",
        };
        return ctx.body;
      }
    } else {
      console.log("No Cutomer id!", input.customer_id);
      try {
        //Invalid ticket command
        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','35')`;
        await client.query(query3);

        await new Promise((resolve) => setTimeout(resolve, 3000));

        //Press or Scan Ticket
        let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','33')`;
        await client.query(query4);
      } catch (err) {
        console.log(err);
      }
    }
  }
})();

//Exit Ticket Manager
(async (ctx, next) => {
  //Get request body
  const input = ctx.request.body;
  //if pass_id present
  if (input && input.pass_id) {
    console.log(input, g_exit_object.exit_flag);
    // fetch tickets/reservation
    if (g_exit_object.exit_flag == false) {
      const query = `select * from reservations where pass_id='${input.pass_id}'`;
      const res = await client.query(query);
      const data1 = res.rows[0];

      if (res.rows.length > 0) {
        //Check reservation status
        if (data1.status_id == 5 || data1.status_id == 1) {
          console.log("Reservation In progress !", input.pass_id);
          try {
            //Get entry time from tickets and amount from reservation table
            const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

            const res = await client.query(query1);
            const temp1 = res.rows[0];
            const count = res.rows.length;
            //validate
            if (count > 0) {
              try {
                let entry_time = new Date(temp1.entry_time).getTime();
                if (temp1.exit_time == null) {
                  console.log("Exit time not present");
                  let exit_time = new Date().getTime();
                  //cal diff between entry and exit time
                  const timeDiff = exit_time - entry_time;
                  // get sec
                  let sec = timeDiff / 1000;

                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(entry_time).add(hh, "hours").format();

                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );
                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                    /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Done!",
                    };
                    return ctx.body;
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);
                    //Update Reservation exit number plate - Completed
                    let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                    await client.query(query11);
                    //Update ticket exit number plate and image
                    let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query12);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Car exiting with 15 min");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Car exiting with 15 min!",
                    };
                    return ctx.body;
                  }
                } else {
                  console.log("Exit time present", temp1.exit_time);
                  let end_time = new Date(temp1.exit_time).getTime();
                  let new_end_time = new Date().getTime();
                  //cal diff between current end time and exit time
                  const timeDiff = new_end_time - end_time;
                  console.log(timeDiff);
                  // get sec
                  let sec = timeDiff / 1000;
                  console.log(sec);
                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(end_time).add(hh, "hours").format();
                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );

                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                      invoice_id: Math.round(new Date().getTime() / 1000),
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Payment is captured");

                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Payment is captured!",
                    };
                    return ctx.body;
                  }
                }
              } catch (error) {
                console.log("Error", error);
              }
            } else {
              console.log("No ticket has been created");
              try {
                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);
              } catch (err) {
                console.log(err);
              }
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "No ticket has been created!",
              };
              return ctx.body;
            }
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Invalid reservation status !",
          };
          return ctx.body;
        }
      } else {
        console.log("No On going Reservation found for passId!", input.pass_id);
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "No On going Reservation found for passId !",
        };
        return ctx.body;
      }
    } else {
      const query = `select * from reservations where pass_id='${input.pass_id}'`;
      const res = await client.query(query);
      const data1 = res.rows[0];
      if (res.rows.length > 0) {
        //Check reservation status
        if (data1.status_id == 5 || data1.status_id == 1) {
          console.log("Reservation In progress !", input.pass_id);
          try {
            //Get entry time from tickets and amount from reservation table
            const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

            const res = await client.query(query1);
            const temp1 = res.rows[0];
            const count = res.rows.length;
            //validate
            if (count > 0) {
              try {
                let entry_time = new Date(temp1.entry_time).getTime();
                if (temp1.exit_time == null) {
                  console.log("Exit time not present");
                  let exit_time = new Date().getTime();
                  //cal diff between entry and exit time
                  const timeDiff = exit_time - entry_time;
                  // get sec
                  let sec = timeDiff / 1000;

                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(entry_time).add(hh, "hours").format();

                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );
                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                    /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Done!",
                    };
                    return ctx.body;
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);
                    //Update Reservation exit number plate - Completed
                    let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                    await client.query(query11);
                    //Update ticket exit number plate and image
                    let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query12);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Car exiting with 15 min");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Car exiting with 15 min!",
                    };
                    return ctx.body;
                  }
                } else {
                  console.log("Exit time present", temp1.exit_time);
                  let end_time = new Date(temp1.exit_time).getTime();
                  let new_end_time = new Date().getTime();
                  //cal diff between current end time and exit time
                  const timeDiff = new_end_time - end_time;
                  console.log(timeDiff);
                  // get sec
                  let sec = timeDiff / 1000;
                  console.log(sec);
                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(end_time).add(hh, "hours").format();
                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );

                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                      invoice_id: Math.round(new Date().getTime() / 1000),
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Payment is captured");

                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Payment is captured!",
                    };
                    return ctx.body;
                  }
                }
              } catch (error) {
                console.log("Error", error);
              }
            } else {
              console.log("No ticket has been created");
              try {
                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);
              } catch (err) {
                console.log(err);
              }
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "No ticket has been created!",
              };
              return ctx.body;
            }
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Invalid reservation status !",
          };
          return ctx.body;
        }
      } else {
        console.log(
          "No On going Reservation found for passId !",
          input.pass_id
        );
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "No On going Reservation found for passId !",
        };
        return ctx.body;
      }
    }
  }
  //if rfid present
  else if (input && input.rfid) {
    console.log("RFID______", input);
    try {
      if (g_exit_object.exit_flag == false) {
        // fetch tickets/reservation
        const query1 = `select * from rfid_tickets where rfid='${input.rfid}' and status_id!=2`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (data1) {
          //Update ticket exit number plate
          let query6 = `update rfid_tickets set exit_number_plate = '${g_exit_object.number_plate}' where rfid='${input.rfid}'`;
          await client.query(query6);

          g_exit_object.exit_flag = true;

          //Insert into commands to open barrier
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
          await client.query(query3);
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Done!",
          };
          return ctx.body;
        } else {
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "RFID ticket was not generated!",
          };
          return ctx.body;
        }
      } else {
        // fetch tickets/reservation
        const query1 = `select * from rfid_tickets where rfid='${input.rfid}' and status_id=1`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (data1) {
          //Update ticket exit time
          let query6 = `update rfid_tickets set status_id = 1 where rfid='${input.rfid}'`;
          await client.query(query6);

          //Insert into commands to open barrier
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
          await client.query(query3);
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Done!",
          };
          return ctx.body;
        } else {
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "RFID ticket was not generated!",
          };
          return ctx.body;
        }
      }
    } catch (err) {
      console.log(err);
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Error",
      };
      return ctx.body;
    }
  }
  //if customer_id present
  else if (input && input.customer_id) {
    // fetch tickets/reservation
    if (g_exit_object.exit_flag == false) {
      const query = `select * from reservations where customer_id='${input.customer_id}'`;
      const res = await client.query(query);
      const data1 = res.rows[0];

      if (res.rows.length > 0) {
        //Check reservation status
        if (data1.status_id == 5 || data1.status_id == 1) {
          console.log("Reservation In progress !", input.pass_id);
          try {
            //Get entry time from tickets and amount from reservation table
            const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

            const res = await client.query(query1);
            const temp1 = res.rows[0];
            const count = res.rows.length;
            //validate
            if (count > 0) {
              try {
                let entry_time = new Date(temp1.entry_time).getTime();
                if (temp1.exit_time == null) {
                  console.log("Exit time not present");
                  let exit_time = new Date().getTime();
                  //cal diff between entry and exit time
                  const timeDiff = exit_time - entry_time;
                  // get sec
                  let sec = timeDiff / 1000;

                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(entry_time).add(hh, "hours").format();

                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );
                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                    /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Done!",
                    };
                    return ctx.body;
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);
                    //Update Reservation exit number plate - Completed
                    let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                    await client.query(query11);
                    //Update ticket exit number plate and image
                    let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query12);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Car exiting with 15 min");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Car exiting with 15 min!",
                    };
                    return ctx.body;
                  }
                } else {
                  console.log("Exit time present", temp1.exit_time);
                  let end_time = new Date(temp1.exit_time).getTime();
                  let new_end_time = new Date().getTime();
                  //cal diff between current end time and exit time
                  const timeDiff = new_end_time - end_time;
                  console.log(timeDiff);
                  // get sec
                  let sec = timeDiff / 1000;
                  console.log(sec);
                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(end_time).add(hh, "hours").format();
                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );

                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                      invoice_id: Math.round(new Date().getTime() / 1000),
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Payment is captured");

                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Payment is captured!",
                    };
                    return ctx.body;
                  }
                }
              } catch (error) {
                console.log("Error", error);
              }
            } else {
              console.log("No ticket has been created");
              try {
                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);
              } catch (err) {
                console.log(err);
              }
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "No ticket has been created!",
              };
              return ctx.body;
            }
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Invalid reservation status !",
          };
          return ctx.body;
        }
      } else {
        console.log("No On going Reservation found for passId!", input.pass_id);
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "No On going Reservation found for passId !",
        };
        return ctx.body;
      }
    } else {
      const query = `select * from reservations where pass_id='${input.pass_id}'`;
      const res = await client.query(query);
      const data1 = res.rows[0];
      if (res.rows.length > 0) {
        //Check reservation status
        if (data1.status_id == 5 || data1.status_id == 1) {
          console.log("Reservation In progress !", input.pass_id);
          try {
            //Get entry time from tickets and amount from reservation table
            const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

            const res = await client.query(query1);
            const temp1 = res.rows[0];
            const count = res.rows.length;
            //validate
            if (count > 0) {
              try {
                let entry_time = new Date(temp1.entry_time).getTime();
                if (temp1.exit_time == null) {
                  console.log("Exit time not present");
                  let exit_time = new Date().getTime();
                  //cal diff between entry and exit time
                  const timeDiff = exit_time - entry_time;
                  // get sec
                  let sec = timeDiff / 1000;

                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(entry_time).add(hh, "hours").format();

                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );
                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                    /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Done!",
                    };
                    return ctx.body;
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);
                    //Update Reservation exit number plate - Completed
                    let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                    await client.query(query11);
                    //Update ticket exit number plate and image
                    let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query12);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Car exiting with 15 min");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Car exiting with 15 min!",
                    };
                    return ctx.body;
                  }
                } else {
                  console.log("Exit time present", temp1.exit_time);
                  let end_time = new Date(temp1.exit_time).getTime();
                  let new_end_time = new Date().getTime();
                  //cal diff between current end time and exit time
                  const timeDiff = new_end_time - end_time;
                  console.log(timeDiff);
                  // get sec
                  let sec = timeDiff / 1000;
                  console.log(sec);
                  if (sec > 300) {
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;
                    const parkingCharges = (hh * temp1.amount).toFixed(2);

                    exit_hour = moment(end_time).add(hh, "hours").format();
                    //cal tax charges for parking charge
                    const taxCharges = (
                      parseFloat(parkingCharges) * 0.15
                    ).toFixed(2);
                    //Add tax charge and parking charge
                    const TotalCharge = (
                      parseFloat(parkingCharges) + parseFloat(taxCharges)
                    ).toFixed(2);
                    console.log(
                      `Total time ${hh} and charges cal is ${TotalCharge}`
                    );

                    //Insert into commands to display amount
                    let cmd = {
                      text: `Please Pay ${TotalCharge} SAR`,
                      invoice_id: Math.round(new Date().getTime() / 1000),
                    };
                    cmd = JSON.stringify(cmd);

                    //Update Reservation exit number plate - Completed
                    let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    //Update ticket exit number plate and image
                    let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                    await client.query(query6);

                    g_exit_object.exit_flag = true;
                    g_entry_object.entry_image = "";
                    g_entry_object.number_plate = "";

                    //Insert into commands to clear
                    let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                    await client.query(query7);
                    //Insert into commands to display amount
                    let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                    await client.query(query8);
                    let cmd1 = {
                      amount: TotalCharge,
                      invoice_id: "0123456789",
                    };
                    cmd1 = JSON.stringify(cmd1);
                    //Insert into commands to trigger POS
                    let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                    await client.query(query34);
                    await new Promise((resolve) => setTimeout(resolve, 200));
                    //Insert commands to send money to POS
                    //Insert into commands
                    let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                    await client.query(query35);
                  } else {
                    //Insert into commands for paymnet succesfull
                    let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                    await client.query(query6);

                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                    await client.query(query3);

                    console.log("Payment is captured");

                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Payment is captured!",
                    };
                    return ctx.body;
                  }
                }
              } catch (error) {
                console.log("Error", error);
              }
            } else {
              console.log("No ticket has been created");
              try {
                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);
              } catch (err) {
                console.log(err);
              }
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "No ticket has been created!",
              };
              return ctx.body;
            }
          } catch (error) {
            console.log(error);
          }
        } else {
          console.log("Invalid reservation status !", input.pass_id);
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Invalid reservation status !",
          };
          return ctx.body;
        }
      } else {
        console.log(
          "No On going Reservation found for passId !",
          input.pass_id
        );
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "No On going Reservation found for passId !",
        };
        return ctx.body;
      }
    }
  }
})();
