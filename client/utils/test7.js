function computeChecksum(input) {
  let temp = '0000';
  for (let k = 0; k < input.length; k = k + 2) {
    const a = temp;
    const b = '00' + input.slice(k, k + 2);
    console.log(b);
    let ndigits = 4,
      i,
      carry = 0,
      d,
      result = '';

    for (i = ndigits - 1; i >= 0; i--) {
      d = parseInt(a[i], 16) + parseInt(b[i], 16) + carry;
      carry = d >> 4;
      result = (d & 15).toString(16) + result;
    }
    temp = result;
  }
  console.log(`Checksum of -> ', ${input} is ${temp.slice(-2)}`);
  return temp.slice(-2);
}
console.log(
  computeChecksum('7F9F001A1303313636333130313738373031313232320D0A')
);
