const FTPClient = require("ftp");
const fs = require("fs");
//const config = require("./src/config/config");
const min = require("./minio");

let ftp_client = new FTPClient();
let ftpConfig = {
  host: "192.168.0.40",
  port: 21,
  user: "admin",
  password: "quercus2",
};
var downloadList = [];

//const id = 2;

let name = "";

//list directory and files from server.
module.exports = {
  ftp: async (id) => {
    console.log(id);
    //create a connection to ftp server
    try {
      ftp_client.connect(ftpConfig);
      console.log("FTP server connected");
      return new Promise((resolve, reject) => {
        ftp_client.on("ready", async function () {
          ftp_client.list("/Log", async function (err, list) {
            if (err) throw err;
            list.map(function (entry) {
              try {
                if (entry != null) {
                  name = entry.name.split(".");
                  //console.log(name[0]);
                  if (name[0] == id && name[1] == "jpg") {
                    downloadList.push(entry.name);
                  }
                  downloadList.map(async function (file, index) {
                    console.log("File", file);
                    // Download remote files and save it to the local file system:
                    ftp_client.get(
                      "Log/" + file,
                      async function (error, stream) {
                        if (error) return err(error);
                        stream.once("close", async function () {
                          ftp_client.end();
                        });
                        stream.pipe(
                          fs.createWriteStream(`${__dirname}\\data\\${file}`)
                        );

                        setTimeout(async () => {
                          console.log("Inside Timeout");
                          let url = await min(file);
                          resolve(url);
                        }, 3000);
                      }
                    );
                  });
                  downloadList = [];
                } else {
                  console.log("Jpg file is not present");
                  return false;
                }
              } catch (error) {
                console.log(error);
              }
            });

            ftp_client.end();
          });
        });
      });
    } catch (error) {
      console.log(error);
    }
  },
};
