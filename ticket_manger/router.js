"use strict";
//Import Koa Router
const Router = require("koa-router");
//Instantiate Router
const router = new Router();
//Import Controller
const Controller = require("./app");

//update exit ticket
router.post("/exit_update_ticket", Controller.exit_update_ticket);
//update ticket
router.post("/update-ticket", Controller.update_ticket);
//Save Logs to DB
router.post("/raw-data", Controller.saveRawData);
//Ticket Manager
router.post("/entry-ticket-manager", Controller.entryTicketManager);
//Ticket Manager - Exit Gate
router.post("/exit-ticket-manager", Controller.exitTicketManager);

//Command Handler
router.post("/command-manager", Controller.sendCommands);

//Regular Parking
router.post("/regular-parking", Controller.regularParking);
//Payment
router.post("/payments", Controller.capturePayment);

//ANPR Handler - OnCarHandled
router.post("/anpr-manager/OnCarHandled", Controller.anprOnCarHandled);
//ANPR Handler - OnCarArrival
router.post("/anpr-manager/OnCarArrival", Controller.anprOnCarArrival);
//ANPR Handler - OnCarDeparture
router.post("/anpr-manager/OnCarDeparture", Controller.anprOnCarDeparture);

//ANPR Handler - OnNotification
router.post("/anpr-manager/OnNotification", Controller.anprOnNotification);
//ANPR Handler - OnInputChange
router.post("/anpr-manager/OnInputChange", Controller.anprOnInputChange);

//Export
module.exports = router;
