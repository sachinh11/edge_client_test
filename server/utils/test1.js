var a = '007F',
  b = '00AD';

var ndigits = 4,
  i,
  carry = 0,
  d,
  result = '';

for (i = ndigits - 1; i >= 0; i--) {
  d = parseInt(a[i], 16) + parseInt(b[i], 16) + carry;
  carry = d >> 4;
  result = (d & 15).toString(16) + result;
}
console.log(result);
