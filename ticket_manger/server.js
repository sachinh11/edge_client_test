'use strict';
//Import Koa
const Koa = require('koa');
//Import Koa Body Parser
const BodyParser = require('koa-bodyparser');
//Import Koa Morgan - Logger
const Morgan = require('koa-morgan');
//Import Koa Router
const Router = require('./router.js');
//Import Koa Cors
const Cors = require('@koa/cors');
//Import Settings
const { appName, port, ip, env } = require('./config/adaptor');
//Koa
class App extends Koa {
  constructor(...params) {
    super(...params);
    this._setMiddlewares();
    this._setRoutes();
  }
  _setMiddlewares() {
    //CORS
    this.use(Cors());
    //Body Parser
    this.use(
      BodyParser({
        enableTypes: ['json'],
        jsonLimit: '10mb',
      })
    );
    //Morgan - Request Logger
    this.use(Morgan('combined'));
  }
  _setRoutes() {
    // Application router
    this.use(Router.routes());
    this.use(Router.allowedMethods());
  }

  //Server listen
  listen(...args) {
    const server = super.listen(...args);
    return server;
  }
}
(async () => {
  //Instantiate App
  const app = new App();
  // Start server
  const server = app.listen(port, ip, () => {
    console.log(
      `\n-------\n${appName} listening on ${ip}:${port}, in ${env}\n-------`
    );
  });
})();
