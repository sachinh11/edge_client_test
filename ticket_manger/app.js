//Import Pouch db
const PouchDB = require("pouchdb");
PouchDB.plugin(require("pouchdb-adapter-memory"));
const db = new PouchDB("spInputCollector", { adapter: "memory" });
//Import Moment
const moment = require("moment");
//Import Configs
const { spdb, edgeController } = require("./config/adaptor");
//Import Axios
const Axios = require("axios");
//Import Node PG
const { Client } = require("pg");
const { ftp } = require("./ftp");
const config = require("./config/settings.dev");
//Import Utility functions
const UtilClass = require("./utils");
const Utils = new UtilClass();
let client = null;

let g_entry_object = {
  number_plate: "",
  entry_image: "",
  entry_flag: false,
};

let g_exit_object = {
  number_plate: "",
  exit_image: "",
  exit_flag: false,
};

let exit_hour = 0;
let carId = "";

(async () => {
  client = new Client(spdb.pg.uri);
  try {
    await client.connect();
    console.log("DataBase connected!");
  } catch (e) {
    console.log("DataBase Not connected", e);
  }
})();

module.exports = class APIHandler {
  //entry update status
  static update_ticket = async (ctx, next) => {
    //validate pass_id is true
    if (ctx.request.body.G_Pass_id.passFlag == true) {
      try {
        // let data = {
        //   pass_id: ctx.request.body.G_Pass_id.pass_id,
        //   carId: carId,
        // };

        // await Utils.entryImage(data);
        let query2 = `UPDATE tickets
                          SET
                          status_id=2
                          WHERE pass_id='${ctx.request.body.G_Pass_id.pass_id}'
                        `;
        await client.query(query2);

        let query3 = `UPDATE reservations
                          SET
                          status_id=5
                          WHERE pass_id='${ctx.request.body.G_Pass_id.pass_id}'
                        `;
        await client.query(query3);

        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Updated!",
        };
        return ctx.body;
      } catch (e) {
        console.log(e);
      }
    }
    //validate rfid is true
    else if (ctx.request.body.G_Pass_id.rfid_flag == true) {
      try {
        // let data = {
        //   pass_id: ctx.request.body.G_Pass_id.pass_id,
        //   carId: carId,
        // };

        // await Utils.entryImage(data);

        let query3 = `UPDATE rfid_tickets
                      SET
                      status_id=1
                      WHERE rfid='${ctx.request.body.G_Pass_id.pass_id}'
                    `;

        await client.query(query3);

        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Updated!",
        };
        return ctx.body;
      } catch (e) {
        console.log(e);
      }
    }
    //validate Customer Flag
    else if (ctx.request.body.G_Pass_id.customerFlag == true) {
      let query2 = `UPDATE tickets
                          SET
                          status_id=2
                          WHERE pass_id='${ctx.request.body.G_Pass_id.pass_id}'
                        `;
      await client.query(query2);

      let query3 = `UPDATE reservations
                          SET
                          status_id=5
                          WHERE pass_id='${ctx.request.body.G_Pass_id.pass_id}'
                        `;
      await client.query(query3);

      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Updated!",
      };
      return ctx.body;
    } else {
      console.log("Both the flag is false");
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Both the flag is false!",
      };
      return ctx.body;
    }
  };
  //exit update status
  static exit_update_ticket = async (ctx, next) => {
    //validate pass_id is true
    if (ctx.request.body.G_Exit_Pass_id.passFlag == true) {
      try {
        // let data = {
        //   pass_id: ctx.request.body.G_Exit_Pass_id.pass_id,
        //   carId: carId,
        // };

        // await Utils.exitImage(data);

        let query2 = `UPDATE tickets
                          SET
                          status_id=3, exit_time = now()
                          WHERE pass_id='${ctx.request.body.G_Exit_Pass_id.pass_id}'
                        `;

        await client.query(query2);

        let query3 = `UPDATE reservations
                          SET
                          status_id=2, end_time = now()
                          WHERE pass_id='${ctx.request.body.G_Exit_Pass_id.pass_id}'
                        `;

        await client.query(query3);

        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Updated!",
        };
        return ctx.body;
      } catch (e) {
        console.log(e);
      }
    }
    //validate rfid is true
    else if (ctx.request.body.G_Exit_Pass_id.rfid_flag == true) {
      try {
        // let data = {
        //   pass_id: ctx.request.body.G_Exit_Pass_id.pass_id,
        //   carId: carId,
        // };

        // await Utils.exitImage(data);

        let query3 = `UPDATE rfid_tickets
                          SET
                          status_id=2, exit_time = now()
                          WHERE rfid='${ctx.request.body.G_Exit_Pass_id.pass_id} and status_id!=2'
                        `;
        await client.query(query3);

        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Updated!",
        };
        return ctx.body;
      } catch (e) {
        console.log(e);
      }
    }
    //validate cutsomer flag
    else if (ctx.request.body.G_Exit_Pass_id.customerFlag == true) {
      try {
        // let data = {
        //   pass_id: ctx.request.body.G_Exit_Pass_id.pass_id,
        //   carId: carId,
        // };

        // await Utils.exitImage(data);

        let query2 = `UPDATE tickets
                          SET
                          status_id=3, exit_time = now()
                          WHERE pass_id='${ctx.request.body.G_Exit_Pass_id.pass_id}'
                        `;

        await client.query(query2);

        let query3 = `UPDATE reservations
                          SET
                          status_id=2, end_time = now()
                          WHERE pass_id='${ctx.request.body.G_Exit_Pass_id.pass_id}'
                        `;

        await client.query(query3);

        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Updated!",
        };
        return ctx.body;
      } catch (e) {
        console.log(e);
      }
    } else {
      console.log("Both the flag is false");
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Both the flag is false!",
      };
      return ctx.body;
    }
  };
  //Save Data logs
  static saveRawData = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    //Insert into raw_data logs
    let query3 = `INSERT INTO raw_data (gate, data) VALUES (${input.gate}, '${input.data}')`;
    await client.query(query3);
    console.log("Raw Data Logged");
    ctx.status = 200;
    ctx.body = {
      success: true,
      msg: "Done!",
    };
    return ctx.body;
  };
  //Entry Ticket Manager
  static entryTicketManager = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log(input);
    //if pass_id present
    if (input && input.pass_id) {
      //Call Ticket generation API
      // fetch tickets/reservation
      const query1 = `select * from reservations where pass_id='${input.pass_id}'`;
      const res = await client.query(query1);
      const data1 = res.rows[0];

      //validate reservation is present or not
      if (res.rows.length > 0) {
        //Check number plate from anpr is already used or not
        if (g_entry_object.entry_flag == false) {
          //Check reservation status
          if (data1.status_id == 5) {
            console.log(
              "Ticket Already generated, In progress !",
              input.pass_id
            );
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Ticket Already generated, In progress !",
            };
            return ctx.body;
          } else if (data1.status_id == 2) {
            console.log("Ticket Already generated, Completed !", input.pass_id);
            return "Ticket Already generated, Completed !";
          } else if (data1.status_id == 1) {
            console.log("Reservation found for passId !", input.pass_id);
            let entry_time = new Date(data1.start_time).getTime();
            let current_entry_time = new Date().getTime();
            let exit_time = new Date(data1.end_time).getTime();
            //cal diff between entry and exit time
            const timeDiff = current_entry_time - entry_time;
            // get sec
            let sec = timeDiff / 1000;
            console.log(sec);
            console.log(current_entry_time, exit_time);
            if (current_entry_time < exit_time) {
              //Customer late from the given time
              if (sec > 300) {
                console.log("Cutomer entry is late");
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1 for late entry");
                    const entry_time_ticket = moment(new Date()).format();
                    const exit_time_ticket = moment(data1.end_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id, exit_time)
                    VALUES ('${data1.id}', '${data1.site_id}',cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                    console.log("Data", query2);
                    await client.query(query2);
                    Utils.logPublisher(
                      2,
                      1,
                      1,
                      `Ticket Created :${data1.pass_id}`
                    );
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Paymnet was not done
                else {
                  try {
                    console.log("Not paid for late entry");
                    const entry_time_ticket = moment(data1.start_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id)
                      VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // Cutomer early to the given time
              else if (sec > -10800 && sec < 0) {
                console.log("cutsomer entry is early");
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1 for early entry");
                    const entry_time_ticket = moment(data1.start_time).format();

                    const exit_time_ticket = moment(data1.end_time).format();
                    //cal diff between entry and exit time
                    const timeDiff = entry_time_ticket - exit_time_ticket;
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;

                    let ex_time = moment(new Date()).add(hh, "hours").format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), now(), '${data1.pass_id}', '${ex_time}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Cutomer not made the paymnet
                else {
                  try {
                    console.log("Not paid for late entry");
                    const entry_time_ticket = moment(new Date()).format();

                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // cutomer reached within the grace period
              else if (sec > 0 && sec < 300) {
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1 within the time");
                    const entry_time_ticket = moment(data1.start_time).format();
                    const exit_time_ticket = moment(data1.end_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id,number_plate,entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Cutomer not made the paymnet
                else {
                  try {
                    console.log("Not paid, within the time");
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, number_plate,entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar), now(), '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5,number_plate=cast(NULLIF('${g_entry_object.number_plate}', 'null') as varchar)  where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    g_entry_object.entry_flag = true;
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // Invalid ticket
              else {
                console.log("Enterd site way to early!", input.pass_id);

                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);

                await new Promise((resolve) => setTimeout(resolve, 3000));

                //Scan Ticket
                let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
                await client.query(query4);

                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "Enterd site way to early!",
                };
                return ctx.body;
              }
            } else {
              console.log("Invalid Ticket", input.pass_id);

              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 5000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','33')`;
              await client.query(query4);

              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Invalid Ticket",
              };
              return ctx.body;
            }
          } else {
            console.log("Invalid reservation status !", input.pass_id);
            return "Invalid reservation status !";
          }
        } else {
          console.log("entry object falg is false");
          //Check reservation status
          if (data1.status_id == 5) {
            console.log(
              "Ticket Already generated, In progress !",
              input.pass_id
            );
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Ticket Already generated, In progress !",
            };
            return ctx.body;
          } else if (data1.status_id == 2) {
            console.log("Ticket Already generated, Completed !", input.pass_id);
            return "Ticket Already generated, Completed !";
          } else if (data1.status_id == 1) {
            console.log("Reservation found for passId !", input.pass_id);
            let entry_time = new Date(data1.start_time).getTime();
            let current_entry_time = new Date().getTime();
            let exit_time = new Date(data1.end_time).getTime();
            //cal diff between entry and exit time
            const timeDiff = current_entry_time - entry_time;
            // get sec
            let sec = timeDiff / 1000;
            console.log(sec);
            if (current_entry_time < exit_time) {
              //Customer late from the given time
              if (sec > 300) {
                // get hours
                let hh =
                  Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                    ? Math.ceil(timeDiff / 1000 / 60 / 60)
                    : 1;
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1 for late entry");
                    const entry_time_ticket = moment(new Date()).format();
                    const exit_time_ticket = moment(data1.end_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
                    VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Paymnet was not done
                else {
                  try {
                    console.log("Not paid for late entry");
                    const entry_time_ticket = moment(data1.start_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id)
                      VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // Cutomer early to the given time
              else if (sec > -10800 && sec < 0) {
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1 for late entry");
                    const entry_time_ticket = moment(data1.start_time).format();

                    const exit_time_ticket = moment(data1.end_time).format();
                    //cal diff between entry and exit time
                    const timeDiff = entry_time_ticket - exit_time_ticket;
                    // get hours
                    let hh =
                      Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                        ? Math.ceil(timeDiff / 1000 / 60 / 60)
                        : 1;

                    let ex_time = moment(new Date()).add(hh, "hours").format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', now(), '${data1.pass_id}', '${ex_time}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Cutomer not made the paymnet
                else {
                  try {
                    console.log("Not paid for late entry");
                    const entry_time_ticket = moment(new Date()).format();

                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // cutomer reached within the grace period
              else if (sec > 0 && sec < 300) {
                console.log("Entry with the time");
                //Cutomer alreday made the paymnet
                if (data1.payment_status_id == 1) {
                  try {
                    console.log("Payment status id is 1");
                    const entry_time_ticket = moment(data1.start_time).format();
                    const exit_time_ticket = moment(data1.end_time).format();
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id, entry_time, pass_id, exit_time)
              VALUES ('${data1.id}', '${data1.site_id}', '${entry_time_ticket}', '${data1.pass_id}', '${exit_time_ticket}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
                //Cutomer not made the paymnet
                else {
                  try {
                    console.log("Not paid, within the time");
                    //Insert ticket
                    let query2 = `INSERT INTO tickets (reservation_id, site_id,entry_time, pass_id)
                VALUES ('${data1.id}', '${data1.site_id}', now(), '${data1.pass_id}')`;
                    await client.query(query2);
                    console.log("Step 1");
                    //Insert into commands to open barrier
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                    await client.query(query3);
                    console.log("Step 2");
                    //Insert commands to close barrier after 1 minute
                    // await new Promise((resolve) => setTimeout(resolve, 2000));
                    //Insert into commands
                    let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','11')`;
                    await client.query(query4);

                    //Update Reservation Status
                    let query5 = `update reservations set status_id = 5 where pass_id='${input.pass_id}'`;
                    await client.query(query5);
                    console.log("Step 4");
                    //Return
                    ctx.status = 200;
                    ctx.body = {
                      success: true,
                      msg: "Ticket has been generated!",
                    };
                    return ctx.body;
                  } catch (err) {
                    console.log(err);
                    console.log("Unable to create ticket!");
                  }
                }
              }
              // Invalid ticket
              else {
                console.log("Enterd site way to early!", input.pass_id);

                //Invalid ticket command
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                await client.query(query3);

                await new Promise((resolve) => setTimeout(resolve, 3000));

                //Scan Ticket
                let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
                await client.query(query4);

                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "Enterd site way to early!",
                };
                return ctx.body;
              }
            } else {
              console.log("Invalid Ticket", input.pass_id);

              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 5000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','33')`;
              await client.query(query4);

              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Invalid Ticket",
              };
              return ctx.body;
            }
          } else {
            console.log("Invalid reservation status !", input.pass_id);
            return "Invalid reservation status !";
          }
        }
      } else {
        console.log("No Reservation for passId !", input.pass_id);
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('912', '${edgeController.entryIP}','${edgeController.entryPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Press or Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('912', '${edgeController.entryIP}','${edgeController.entryPort}','33')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
      }
    }
    //if rfid present
    else if (input && input.rfid) {
      console.log("----------RFID Scan", input);
      try {
        if (g_entry_object.entry_flag == false) {
          // check rfid id there or not
          const query1 = `select * from smart_tags where uid='${input.rfid}'`;
          const res = await client.query(query1);
          const data1 = res.rows[0];
          if (data1) {
            //check any active ticket availbale for the rfid sub
            const query11 = `select * from rfid_tickets where rfid='${input.rfid}' and status_id =2`;
            const resp = await client.query(query11);
            const data2 = resp.rows[0];
            //validate count no of tickets
            if (resp.rows.length == 0) {
              try {
                //Insert rfid ticket
                let query2 = `INSERT INTO rfid_tickets(rfid, status_id, entry_time, number_plate)
              VALUES ('${data1.uid}', 3, now(), '${g_entry_object.number_plate}')`;
                await client.query(query2);

                //Insert into commands to open barrier
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                await client.query(query3);

                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "Valid RFID!",
                };
                return ctx.body;
              } catch (err) {
                console.log(err);
                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "Error !",
                };
              }
            } else {
              console.log("Active sub alreday inside the site");
              Utils.logPublisher(
                2,
                1,
                1,
                `Active subscriber alreday inside the site`
              );
              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Active sub alreday inside the site!",
              };
              return ctx.body;
            }
          } else {
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "RFID not registered!",
            };
            return ctx.body;
          }
        } else {
          // fetch tickets/reservation
          const query1 = `select * from smart_tags where uid='${input.rfid}'`;
          const res = await client.query(query1);
          const data1 = res.rows[0];
          if (data1) {
            try {
              //Insert rfid ticket
              let query2 = `INSERT INTO rfid_tickets(rfid, status_id, entry_time)
              VALUES ('${data1.uid}', 1, now())`;
              await client.query(query2);

              //Insert into commands to open barrier
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
              await client.query(query3);
              Utils.logPublisher(2, 1, 1, `Barrier oppened`);

              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Valid RFID!",
              };
              return ctx.body;
            } catch (err) {
              console.log(err);
              //Return
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Error !",
              };
            }
          } else {
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "RFID not registered!",
            };
            return ctx.body;
          }
        }
      } catch (err) {
        console.log(err);
        //Return
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Error !",
        };
      }
    }
    //if customer_id present
    else if (input && input.customer_id) {
      // fetch tickets/reservation
      const query50 = `select * from customers where customer_id='${input.customer_id}'`;
      const resp = await client.query(query50);
      const cid = resp.rows[0];

      //validate cutomer is present or not
      if (resp.rows.length > 0) {
        try {
          //Generate passId with current time
          const passId = new Date().valueOf();
          const data = {
            vehicle_id: input.vehicle_id || null,
            site_parking_slot_id: config.site_parking_slot_id,
            reservation_type_id: config.reservation_type_id,
            end_time: null,
            site_id: config.site_id,
            status_id: config.status_id,
            pass_id: passId.toString(),
          };

          //Check number plate from anpr is already used or not
          if (g_entry_object.entry_flag == false) {
            //Insert reservation
            let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount, number_plate, customer_id) 
                  VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100, '${g_entry_object.number_plate}', '${cid.customer_id}')`;
            await client.query(query);
            //Get Reservation Details
            const query2 = `select * from reservations where pass_id='${data.pass_id}'`;
            const res1 = await client.query(query2);
            const data2 = res1.rows[0];
            if (res1.rows.length > 0) {
              try {
                //Insert ticket
                let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, number_plate,entry_time, pass_id, status_id, customer_id)
                    VALUES ('${data2.id}', '${data2.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), '${g_entry_object.number_plate}', now(),'${data2.pass_id}', 4, '${cid.customer_id}')`;
                await client.query(query2);
                let cmd = {
                  url: `https://demo.website.sp.hyperthings.in/ticket/${data2.pass_id}`,
                  pass_id: data2.pass_id,
                };
                cmd = JSON.stringify(cmd);

                //Insert Command to print tickets
                let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
                await client.query(query6);
                await new Promise((resolve) => setTimeout(resolve, 500));
                //Insert into commands to open barrier
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                await client.query(query3);
                Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                //Insert commands to close barrier after 1 minute
                // setTimeout(async () => {
                //   //Insert into commands
                //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
                //   await client.query(query4);
                // }, 3000);
                //Update Reservation Status
                let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
                await client.query(query5);
                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  // msg: `Ticket and Command has been generated - ${data.pass_id}`,
                  msg: `${data.pass_id}`,
                };
                g_entry_object.entry_flag = true;
                return ctx.body;
              } catch (err) {
                console.log(err);
                console.log("Unable to create ticket!");
                ctx.status = 404;
                ctx.body = {
                  success: false,
                  msg: "Unable to create ticket!",
                };
                return ctx.body;
              }
            } else {
              console.log("No Reservation found for passId !", data.pass_id);
            }
          } else {
            //Insert reservation
            let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount, customer_id) 
                  VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100, '${cid.customer_id}')`;

            await client.query(query);
            //Get Reservation Details
            const query2 = `select * from reservations where pass_id='${data.pass_id}'`;
            const res1 = await client.query(query2);
            const data2 = res1.rows[0];
            if (res1.rows.length > 0) {
              try {
                //Insert ticket
                let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, entry_time, pass_id, status_id, customer_id)
                    VALUES ('${data2.id}', '${data2.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), now(),'${data2.pass_id}', 4, '${cid.customer_id}')`;
                await client.query(query2);
                let cmd = {
                  url: `https://demo.website.sp.hyperthings.in/ticket/${data2.pass_id}`,
                  pass_id: data2.pass_id,
                };
                cmd = JSON.stringify(cmd);

                //Insert Command to print tickets
                let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
                await client.query(query6);
                await new Promise((resolve) => setTimeout(resolve, 500));
                //Insert into commands to open barrier
                let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data2.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
                await client.query(query3);
                Utils.logPublisher(2, 1, 1, `Barrier oppened`);
                //Insert commands to close barrier after 1 minute
                // setTimeout(async () => {
                //   //Insert into commands
                //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
                //   await client.query(query4);
                // }, 3000);
                //Update Reservation Status
                let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
                await client.query(query5);
                //Return
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  // msg: `Ticket and Command has been generated - ${data.pass_id}`,
                  msg: `${data.pass_id}`,
                };
                g_entry_object.entry_flag = true;
                return ctx.body;
              } catch (err) {
                console.log(err);
                console.log("Unable to create ticket!");
                ctx.status = 404;
                ctx.body = {
                  success: false,
                  msg: "Unable to create ticket!",
                };
                return ctx.body;
              }
            } else {
              console.log("No Reservation found for passId !", data.pass_id);
            }
          }
        } catch (error) {
          console.log(error);
          console.log("Unable to create reservation!");
          ctx.status = 404;
          ctx.body = {
            success: false,
            msg: "Unable to create reservation!",
          };
          return ctx.body;
        }
      } else {
        console.log("No Cutomer id!", input.customer_id);
        try {
          //Invalid ticket command
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('912', '${edgeController.entryIP}','${edgeController.entryPort}','35')`;
          await client.query(query3);

          await new Promise((resolve) => setTimeout(resolve, 3000));

          //Press or Scan Ticket
          let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('912', '${edgeController.entryIP}','${edgeController.entryPort}','33')`;
          await client.query(query4);
        } catch (err) {
          console.log(err);
        }
      }
    }
  };
  //Exit Ticket Manager
  static exitTicketManager = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    //if pass_id present
    if (input && input.pass_id) {
      // fetch tickets/reservation
      if (g_exit_object.exit_flag == false) {
        const query = `select * from reservations where pass_id='${input.pass_id}'`;
        const res = await client.query(query);
        const data1 = res.rows[0];

        if (res.rows.length > 0) {
          //Check reservation status
          if (data1.status_id == 5 || data1.status_id == 1) {
            console.log("Reservation In progress !", input.pass_id);
            if (data1.payment_status_id == 5) {
              Utils.logPublisher(2, 1, 2, `Payment successful`);
              //Insert into commands for paymnet nullify
              let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
              await client.query(query6);
              Utils.logPublisher(2, 1, 2, `Barrier oppened`);
            } else {
              try {
                //Get entry time from tickets and amount from reservation table
                const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                      inner join reservations r on r.id = t.reservation_id
                      where t.pass_id='${input.pass_id}'`;

                const res = await client.query(query1);
                const temp1 = res.rows[0];
                const count = res.rows.length;
                //validate
                if (count > 0) {
                  try {
                    let entry_time = new Date(temp1.entry_time).getTime();
                    if (temp1.exit_time == null) {
                      console.log("Exit time not present");
                      let exit_time = new Date().getTime();
                      //cal diff between entry and exit time
                      const timeDiff = exit_time - entry_time;
                      // get sec
                      let sec = timeDiff / 1000;

                      if (sec > 300) {
                        // get hours
                        let hh =
                          Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                            ? Math.ceil(timeDiff / 1000 / 60 / 60)
                            : 1;
                        const parkingCharges = (hh * temp1.amount).toFixed(2);

                        exit_hour = moment(entry_time)
                          .add(hh, "hours")
                          .format();

                        //cal tax charges for parking charge
                        const taxCharges = (
                          parseFloat(parkingCharges) * 0.15
                        ).toFixed(2);
                        //Add tax charge and parking charge
                        const TotalCharge = (
                          parseFloat(parkingCharges) + parseFloat(taxCharges)
                        ).toFixed(2);
                        console.log(
                          `Total time ${hh} and charges cal is ${TotalCharge}`
                        );
                        //Insert into commands to display amount
                        let cmd = {
                          text: `Please Pay ${TotalCharge} SAR`,
                        };
                        cmd = JSON.stringify(cmd);

                        //Update Reservation exit number plate - Completed
                        let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query5);
                        //Update ticket exit number plate and image
                        let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query6);

                        g_exit_object.exit_flag = true;
                        g_entry_object.entry_image = "";
                        g_entry_object.number_plate = "";

                        //Insert into commands to clear
                        let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                        await client.query(query7);
                        //Insert into commands to display amount
                        let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                        await client.query(query8);
                        let cmd1 = {
                          amount: TotalCharge,
                          invoice_id: "0123456789",
                        };
                        cmd1 = JSON.stringify(cmd1);
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) =>
                          setTimeout(resolve, 200)
                        );
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                        await client.query(query35);
                        Utils.logPublisher(
                          2,
                          1,
                          2,
                          `Customer has to Pay ${TotalCharge}`
                        );
                        /*
                          //New Payment Setup
                          //Insert into commands to trigger POS
                          let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                          await client.query(query34);
                          await new Promise((resolve) => setTimeout(resolve, 200));
                          //Insert commands to send money to POS
                          //Insert into commands
                          let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;
  
                          */

                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Done!",
                        };
                        return ctx.body;
                      } else {
                        //Insert into commands for paymnet succesfull
                        let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                        await client.query(query6);
                        Utils.logPublisher(2, 1, 2, `Payment Successful`);
                        //Update Reservation exit number plate - Completed
                        let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                        await client.query(query11);
                        //Update ticket exit number plate and image
                        let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query12);

                        Utils.logPublisher(
                          2,
                          1,
                          2,
                          `CAR is Exiting within 15 minutes`
                        );

                        //Insert into commands to open barrier
                        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                        await client.query(query3);

                        Utils.logPublisher(2, 1, 2, `Barrier oppened`);

                        console.log("Car exiting with 15 min");
                        //Return
                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Car exiting with 15 min!",
                        };
                        return ctx.body;
                      }
                    } else {
                      console.log("Exit time present", temp1.exit_time);
                      let end_time = new Date(temp1.exit_time).getTime();
                      let new_end_time = new Date().getTime();
                      //cal diff between current end time and exit time
                      const timeDiff = new_end_time - end_time;
                      console.log(timeDiff);
                      // get sec
                      let sec = timeDiff / 1000;
                      console.log(sec);
                      if (sec > 300) {
                        // get hours
                        let hh =
                          Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                            ? Math.ceil(timeDiff / 1000 / 60 / 60)
                            : 1;
                        const parkingCharges = (hh * temp1.amount).toFixed(2);

                        exit_hour = moment(end_time).add(hh, "hours").format();
                        //cal tax charges for parking charge
                        const taxCharges = (
                          parseFloat(parkingCharges) * 0.15
                        ).toFixed(2);
                        //Add tax charge and parking charge
                        const TotalCharge = (
                          parseFloat(parkingCharges) + parseFloat(taxCharges)
                        ).toFixed(2);
                        console.log(
                          `Total time ${hh} and charges cal is ${TotalCharge}`
                        );

                        //Insert into commands to display amount
                        let cmd = {
                          text: `Please Pay ${TotalCharge} SAR`,
                          invoice_id: Math.round(new Date().getTime() / 1000),
                        };
                        cmd = JSON.stringify(cmd);

                        //Update Reservation exit number plate - Completed
                        let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query5);
                        //Update ticket exit number plate and image
                        let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query6);

                        g_exit_object.exit_flag = true;
                        g_entry_object.entry_image = "";
                        g_entry_object.number_plate = "";

                        let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                        await client.query(query7);
                        //Insert into commands to display amount
                        let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                        await client.query(query8);
                        let cmd1 = {
                          amount: TotalCharge,
                          invoice_id: "0123456789",
                        };
                        cmd1 = JSON.stringify(cmd1);
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) =>
                          setTimeout(resolve, 200)
                        );
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                        await client.query(query35);
                      } else {
                        Utils.logPublisher(
                          2,
                          1,
                          2,
                          `Customer has to Pay ${TotalCharge}`
                        );
                        //Insert into commands for paymnet succesfull
                        let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                        await client.query(query6);
                        Utils.logPublisher(2, 1, 2, `Payment Successful`);
                        //Insert into commands to open barrier
                        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                        await client.query(query3);
                        Utils.logPublisher(2, 1, 2, `Barrier oppened`);

                        console.log("Payment is captured");

                        //Return
                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Payment is captured!",
                        };
                        return ctx.body;
                      }
                    }
                  } catch (error) {
                    console.log("Error", error);
                  }
                } else {
                  console.log("No ticket has been created");
                  try {
                    Utils.logPublisher(2, 1, 2, `Invalid Ticket scanned`);
                    //Invalid ticket command
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                    await client.query(query3);
                  } catch (err) {
                    console.log(err);
                  }
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "No ticket has been created!",
                  };
                  return ctx.body;
                }
              } catch (error) {
                console.log(error);
              }
            }
          } else {
            console.log("Invalid reservation status !", input.pass_id);
            try {
              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);
              Utils.logPublisher(2, 1, 2, `Invalid Ticket scanned`);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);
            } catch (err) {
              console.log(err);
            }
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid reservation status !",
            };
            return ctx.body;
          }
        } else {
          console.log(
            "No On going Reservation found for passId!",
            input.pass_id
          );
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);
            Utils.logPublisher(2, 1, 2, `Invalid Ticket scanned`);
            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "No On going Reservation found for passId !",
          };
          return ctx.body;
        }
      } else {
        const query = `select * from reservations where pass_id='${input.pass_id}'`;
        const res = await client.query(query);
        const data1 = res.rows[0];
        if (res.rows.length > 0) {
          //Check reservation status
          if (data1.status_id == 5 || data1.status_id == 1) {
            console.log("Reservation In progress !", input.pass_id);
            if (data1.payment_status_id == 5) {
              //Insert into commands for paymnet nullify
              Utils.logPublisher(2, 1, 2, `Payment successfull`);
              let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
              await client.query(query6);
              Utils.logPublisher(2, 1, 2, `Barrier oppened`);
            } else {
              try {
                //Get entry time from tickets and amount from reservation table
                const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

                const res = await client.query(query1);
                const temp1 = res.rows[0];
                const count = res.rows.length;
                //validate
                if (count > 0) {
                  try {
                    let entry_time = new Date(temp1.entry_time).getTime();
                    if (temp1.exit_time == null) {
                      console.log("Exit time not present");
                      let exit_time = new Date().getTime();
                      //cal diff between entry and exit time
                      const timeDiff = exit_time - entry_time;
                      // get sec
                      let sec = timeDiff / 1000;

                      if (sec > 300) {
                        // get hours
                        let hh =
                          Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                            ? Math.ceil(timeDiff / 1000 / 60 / 60)
                            : 1;
                        const parkingCharges = (hh * temp1.amount).toFixed(2);

                        exit_hour = moment(entry_time)
                          .add(hh, "hours")
                          .format();

                        //cal tax charges for parking charge
                        const taxCharges = (
                          parseFloat(parkingCharges) * 0.15
                        ).toFixed(2);
                        //Add tax charge and parking charge
                        const TotalCharge = (
                          parseFloat(parkingCharges) + parseFloat(taxCharges)
                        ).toFixed(2);
                        console.log(
                          `Total time ${hh} and charges cal is ${TotalCharge}`
                        );
                        //Insert into commands to display amount
                        let cmd = {
                          text: `Please Pay ${TotalCharge} SAR`,
                        };
                        cmd = JSON.stringify(cmd);

                        //Update Reservation exit number plate - Completed
                        let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query5);
                        //Update ticket exit number plate and image
                        let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query6);

                        g_exit_object.exit_flag = true;
                        g_entry_object.entry_image = "";
                        g_entry_object.number_plate = "";

                        //Insert into commands to clear
                        let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                        await client.query(query7);
                        //Insert into commands to display amount
                        let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                        await client.query(query8);
                        let cmd1 = {
                          amount: TotalCharge,
                          invoice_id: "0123456789",
                        };
                        cmd1 = JSON.stringify(cmd1);
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) =>
                          setTimeout(resolve, 200)
                        );
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                        await client.query(query35);
                        Utils.logPublisher(
                          2,
                          1,
                          2,
                          `Customer has to Pay ${TotalCharge}`
                        );
                        /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Done!",
                        };
                        return ctx.body;
                      } else {
                        //Insert into commands for paymnet succesfull
                        let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                        await client.query(query6);
                        Utils.logPublisher(2, 1, 2, `Payment Successful`);
                        //Update Reservation exit number plate - Completed
                        let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${input.pass_id}'`;
                        await client.query(query11);
                        //Update ticket exit number plate and image
                        let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query12);

                        //Insert into commands to open barrier
                        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                        await client.query(query3);
                        Utils.logPublisher(2, 1, 2, `Car exiting with 15 min`);

                        console.log("Car exiting with 15 min");
                        //Return
                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Car exiting with 15 min!",
                        };
                        return ctx.body;
                      }
                    } else {
                      console.log("Exit time present", temp1.exit_time);
                      let end_time = new Date(temp1.exit_time).getTime();
                      let new_end_time = new Date().getTime();
                      //cal diff between current end time and exit time
                      const timeDiff = new_end_time - end_time;
                      console.log(timeDiff);
                      // get sec
                      let sec = timeDiff / 1000;
                      console.log(sec);
                      if (sec > 300) {
                        // get hours
                        let hh =
                          Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                            ? Math.ceil(timeDiff / 1000 / 60 / 60)
                            : 1;
                        const parkingCharges = (hh * temp1.amount).toFixed(2);

                        exit_hour = moment(end_time).add(hh, "hours").format();
                        //cal tax charges for parking charge
                        const taxCharges = (
                          parseFloat(parkingCharges) * 0.15
                        ).toFixed(2);
                        //Add tax charge and parking charge
                        const TotalCharge = (
                          parseFloat(parkingCharges) + parseFloat(taxCharges)
                        ).toFixed(2);
                        console.log(
                          `Total time ${hh} and charges cal is ${TotalCharge}`
                        );

                        //Insert into commands to display amount
                        let cmd = {
                          text: `Please Pay ${TotalCharge} SAR`,
                          invoice_id: Math.round(new Date().getTime() / 1000),
                        };
                        cmd = JSON.stringify(cmd);

                        //Update Reservation exit number plate - Completed
                        let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query5);
                        //Update ticket exit number plate and image
                        let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${input.pass_id}'`;
                        await client.query(query6);

                        g_exit_object.exit_flag = true;
                        g_entry_object.entry_image = "";
                        g_entry_object.number_plate = "";

                        //Insert into commands to clear
                        let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                        await client.query(query7);
                        //Insert into commands to display amount
                        let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                        await client.query(query8);
                        let cmd1 = {
                          amount: TotalCharge,
                          invoice_id: "0123456789",
                        };
                        cmd1 = JSON.stringify(cmd1);
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) =>
                          setTimeout(resolve, 200)
                        );
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                        await client.query(query35);
                        Utils.logPublisher(
                          2,
                          1,
                          2,
                          `Customer has to Pay ${TotalCharge}`
                        );
                      } else {
                        //Insert into commands for paymnet succesfull
                        let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                        await client.query(query6);
                        Utils.logPublisher(2, 1, 2, `Payment Successful`);
                        //Insert into commands to open barrier
                        let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                        await client.query(query3);

                        console.log("Payment is captured");

                        //Return
                        ctx.status = 200;
                        ctx.body = {
                          success: true,
                          msg: "Payment is captured!",
                        };
                        return ctx.body;
                      }
                    }
                  } catch (error) {
                    console.log("Error", error);
                  }
                } else {
                  console.log("No ticket has been created");
                  try {
                    //Invalid ticket command
                    let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                    await client.query(query3);
                  } catch (err) {
                    console.log(err);
                  }
                  ctx.status = 200;
                  ctx.body = {
                    success: true,
                    msg: "No ticket has been created!",
                  };
                  return ctx.body;
                }
              } catch (error) {
                console.log(error);
              }
            }
          } else {
            console.log("Invalid reservation status !", input.pass_id);
            try {
              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);
            } catch (err) {
              console.log(err);
            }
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid reservation status !",
            };
            return ctx.body;
          }
        } else {
          console.log(
            "No On going Reservation found for passId !",
            input.pass_id
          );
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "No On going Reservation found for passId !",
          };
          return ctx.body;
        }
      }
    }
    //if rfid present
    else if (input && input.rfid) {
      console.log("RFID______", input, g_exit_object.exit_flag);
      try {
        if (g_exit_object.exit_flag == false) {
          // check rfid id there or not
          const query11 = `select * from smart_tags where uid='${input.rfid}'`;
          const resp = await client.query(query11);
          const data2 = resp.rows[0];
          console.log(data2);
          // fetch tickets/reservation
          const query1 = `select * from rfid_tickets where rfid='${input.rfid}' and status_id!=1`;
          const res = await client.query(query1);
          const data1 = res.rows[0];
          console.log(data1);
          if (data1) {
            let entry_time = new Date(data1.entry_time).getTime();
            if (data1.exit_time == null && data2.thr > 0) {
              console.log("Exit time not present");
              let exit_time = new Date().getTime();
              //cal diff between entry and exit time and get min
              const timeDiff = Math.round(
                (Math.abs(exit_time - entry_time) / 1000) * 60
              );

              console.log(`Total min ${timeDiff}`);

              const tHr = data2.thr - timeDiff;

              //Update ticket exit number plate
              let query6 = `update rfid_tickets set exit_number_plate = '${g_exit_object.number_plate}', hours='${timeDiff}' where rfid='${input.rfid}'`;
              console.log(query6);
              await client.query(query6);

              //Update Hours in smart Tag
              let query9 = `update smart_tags set thr=${tHr} where uid='${input.rfid}'`;
              console.log(query9);
              await client.query(query9);

              g_exit_object.exit_flag = true;
              g_entry_object.entry_image = "";
              g_entry_object.number_plate = "";

              //Insert into commands to open barrier
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
              await client.query(query3);
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Done!",
              };
              return ctx.body;
            } else {
              console.log("Exit time is not null or Total Hr is exhausted");
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Exit time is not null or Total Hr is exhausted",
              };
              return ctx.body;
            }
          } else {
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "RFID ticket was not generated!",
            };
            return ctx.body;
          }
        } else {
          // check rfid id there or not
          const query11 = `select * from smart_tags where uid='${input.rfid}'`;
          const resp = await client.query(query11);
          const data2 = resp.rows[0];
          // fetch tickets/reservation
          const query1 = `select * from rfid_tickets where rfid='${input.rfid}' and status_id!=2`;
          const res = await client.query(query1);
          const data1 = res.rows[0];
          if (data1) {
            let entry_time = new Date(data1.entry_time).getTime();
            if (data1.exit_time == null && data2.thr > 0) {
              console.log("Exit time not present");
              let exit_time = new Date().getTime();
              //cal diff between entry and exit time and get min
              const timeDiff = Math.round(
                (Math.abs(exit_time - entry_time) / 1000) * 60
              );

              console.log(`Total min ${timeDiff}`);

              const tHr = data2.totalHr - timeDiff;

              //Update ticket exit number plate
              let query6 = `update rfid_tickets set hours='${timeDiff}' where rfid='${input.rfid}'`;
              await client.query(query6);

              //Update Hours in smart Tag
              let query9 = `update smart_tags set thr='${tHr}' where uid='${input.rfid}'`;
              await client.query(query9);

              g_exit_object.exit_flag = true;
              g_entry_object.entry_image = "";
              g_entry_object.number_plate = "";

              //Insert into commands to open barrier
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
              await client.query(query3);
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Done!",
              };
              return ctx.body;
            } else {
              console.log("Exit time is not null or Total Hr is exhausted");
              ctx.status = 200;
              ctx.body = {
                success: true,
                msg: "Exit time is not null or Total Hr is exhausted",
              };
              return ctx.body;
            }
          } else {
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "RFID ticket was not generated!",
            };
            return ctx.body;
          }
        }
      } catch (err) {
        console.log(err);
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Error",
        };
        return ctx.body;
      }
    }
    //if customer_id present
    else if (input && input.customer_id) {
      const query = `select * from reservations where customer_id='${input.customer_id}' and status_id=5`;
      const res = await client.query(query);
      const data1 = res.rows[0];
      // fetch tickets/reservation
      if (g_exit_object.exit_flag == false) {
        if (res.rows.length > 0) {
          //Check reservation status
          if (data1.status_id == 5 || data1.status_id == 1) {
            console.log("Reservation In progress !", data1.pass_id);
            try {
              //Get entry time from tickets and amount from reservation table
              const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${data1.pass_id}'`;

              const res = await client.query(query1);
              const temp1 = res.rows[0];
              const count = res.rows.length;
              //validate
              if (count > 0) {
                try {
                  let entry_time = new Date(temp1.entry_time).getTime();
                  if (temp1.exit_time == null) {
                    console.log("Exit time not present");
                    let exit_time = new Date().getTime();
                    //cal diff between entry and exit time
                    const timeDiff = exit_time - entry_time;
                    // get sec
                    let sec = timeDiff / 1000;

                    if (sec > 300) {
                      // get hours
                      let hh =
                        Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                          ? Math.ceil(timeDiff / 1000 / 60 / 60)
                          : 1;
                      const parkingCharges = (hh * temp1.amount).toFixed(2);

                      exit_hour = moment(entry_time).add(hh, "hours").format();

                      //cal tax charges for parking charge
                      const taxCharges = (
                        parseFloat(parkingCharges) * 0.15
                      ).toFixed(2);
                      //Add tax charge and parking charge
                      const TotalCharge = (
                        parseFloat(parkingCharges) + parseFloat(taxCharges)
                      ).toFixed(2);
                      console.log(
                        `Total time ${hh} and charges cal is ${TotalCharge}`
                      );
                      //Insert into commands to display amount
                      let cmd = {
                        text: `Please Pay ${TotalCharge} SAR`,
                      };
                      cmd = JSON.stringify(cmd);
                      console.log("Update the Tixket");
                      //Update Reservation exit number plate - Completed
                      let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      console.log(query5);
                      await client.query(query5);
                      //Update ticket exit number plate and image
                      let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query6);

                      g_exit_object.exit_flag = true;
                      g_entry_object.entry_image = "";
                      g_entry_object.number_plate = "";

                      //Insert into commands to clear
                      let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                      await client.query(query7);
                      //Insert into commands to display amount
                      let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                      await client.query(query8);
                      let cmd1 = {
                        amount: TotalCharge,
                        invoice_id: "0123456789",
                      };
                      cmd1 = JSON.stringify(cmd1);
                      //Insert into commands to trigger POS
                      let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                      await client.query(query34);
                      await new Promise((resolve) => setTimeout(resolve, 200));
                      //Insert commands to send money to POS
                      //Insert into commands
                      let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                      await client.query(query35);
                      Utils.logPublisher(
                        2,
                        1,
                        2,
                        `Customer has to Pay ${TotalCharge}`
                      );
                      /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        //passid to close the ticket
                        id: `${data1.pass_id}`,
                      };
                      return ctx.body;
                    } else {
                      //Insert into commands for paymnet succesfull
                      let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                      await client.query(query6);
                      Utils.logPublisher(2, 1, 2, `Payment Successful`);
                      //Update Reservation exit number plate - Completed
                      let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${data1.pass_id}'`;
                      await client.query(query11);
                      //Update ticket exit number plate and image
                      let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query12);

                      //Insert into commands to open barrier
                      let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                      await client.query(query3);

                      console.log("Car exiting with 15 min");
                      //Return
                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        msg: "Car exiting with 15 min!",
                        //passiD to close ticket
                        id: `${data1.pass_id}`,
                      };
                      return ctx.body;
                    }
                  } else {
                    console.log("Exit time present", temp1.exit_time);
                    let end_time = new Date(temp1.exit_time).getTime();
                    let new_end_time = new Date().getTime();
                    //cal diff between current end time and exit time
                    const timeDiff = new_end_time - end_time;
                    console.log(timeDiff);
                    // get sec
                    let sec = timeDiff / 1000;
                    console.log(sec);
                    if (sec > 300) {
                      // get hours
                      let hh =
                        Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                          ? Math.ceil(timeDiff / 1000 / 60 / 60)
                          : 1;
                      const parkingCharges = (hh * temp1.amount).toFixed(2);

                      exit_hour = moment(end_time).add(hh, "hours").format();
                      //cal tax charges for parking charge
                      const taxCharges = (
                        parseFloat(parkingCharges) * 0.15
                      ).toFixed(2);
                      //Add tax charge and parking charge
                      const TotalCharge = (
                        parseFloat(parkingCharges) + parseFloat(taxCharges)
                      ).toFixed(2);
                      console.log(
                        `Total time ${hh} and charges cal is ${TotalCharge}`
                      );

                      //Insert into commands to display amount
                      let cmd = {
                        text: `Please Pay ${TotalCharge} SAR`,
                        invoice_id: Math.round(new Date().getTime() / 1000),
                      };
                      cmd = JSON.stringify(cmd);

                      //Update Reservation exit number plate - Completed
                      let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query5);
                      //Update ticket exit number plate and image
                      let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query6);

                      g_exit_object.exit_flag = true;
                      g_entry_object.entry_image = "";
                      g_entry_object.number_plate = "";

                      let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                      await client.query(query7);
                      //Insert into commands to display amount
                      let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                      await client.query(query8);
                      let cmd1 = {
                        amount: TotalCharge,
                        invoice_id: "0123456789",
                      };
                      cmd1 = JSON.stringify(cmd1);
                      //Insert into commands to trigger POS
                      let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                      await client.query(query34);
                      await new Promise((resolve) => setTimeout(resolve, 200));
                      //Insert commands to send money to POS
                      //Insert into commands
                      let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                      await client.query(query35);
                      Utils.logPublisher(
                        2,
                        1,
                        2,
                        `Customer has to Pay ${TotalCharge}`
                      );
                    } else {
                      //Insert into commands for paymnet succesfull
                      let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                      await client.query(query6);
                      Utils.logPublisher(2, 1, 2, `Payment Successful`);
                      //Insert into commands to open barrier
                      let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                      await client.query(query3);

                      console.log("Payment is captured");

                      //Return
                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        msg: "Payment is captured!",
                      };
                      return ctx.body;
                    }
                  }
                } catch (error) {
                  console.log("Error", error);
                }
              } else {
                console.log("No ticket has been created");
                try {
                  //Invalid ticket command
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                  await client.query(query3);
                } catch (err) {
                  console.log(err);
                }
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "No ticket has been created!",
                };
                return ctx.body;
              }
            } catch (error) {
              console.log(error);
            }
          } else {
            console.log("Invalid reservation status !", data1.pass_id);
            try {
              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);
            } catch (err) {
              console.log(err);
            }
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid reservation status !",
            };
            return ctx.body;
          }
        } else {
          console.log(
            "No On going Reservation found for passId!",
            input.pass_id
          );
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "No On going Reservation found for passId !",
          };
          return ctx.body;
        }
      } else {
        if (res.rows.length > 0) {
          //Check reservation status
          if (data1.status_id == 5 || data1.status_id == 1) {
            console.log("Reservation In progress !", data1.pass_id);
            try {
              //Get entry time from tickets and amount from reservation table
              const query1 = `select t.entry_time, r.amount, t.exit_time from tickets t 
                    inner join reservations r on r.id = t.reservation_id
                    where t.pass_id='${input.pass_id}'`;

              const res = await client.query(query1);
              const temp1 = res.rows[0];
              const count = res.rows.length;
              //validate
              if (count > 0) {
                try {
                  let entry_time = new Date(temp1.entry_time).getTime();
                  if (temp1.exit_time == null) {
                    console.log("Exit time not present");
                    let exit_time = new Date().getTime();
                    //cal diff between entry and exit time
                    const timeDiff = exit_time - entry_time;
                    // get sec
                    let sec = timeDiff / 1000;

                    if (sec > 300) {
                      // get hours
                      let hh =
                        Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                          ? Math.ceil(timeDiff / 1000 / 60 / 60)
                          : 1;
                      const parkingCharges = (hh * temp1.amount).toFixed(2);

                      exit_hour = moment(entry_time).add(hh, "hours").format();

                      //cal tax charges for parking charge
                      const taxCharges = (
                        parseFloat(parkingCharges) * 0.15
                      ).toFixed(2);
                      //Add tax charge and parking charge
                      const TotalCharge = (
                        parseFloat(parkingCharges) + parseFloat(taxCharges)
                      ).toFixed(2);
                      console.log(
                        `Total time ${hh} and charges cal is ${TotalCharge}`
                      );
                      //Insert into commands to display amount
                      let cmd = {
                        text: `Please Pay ${TotalCharge} SAR`,
                      };
                      cmd = JSON.stringify(cmd);

                      //Update Reservation exit number plate - Completed
                      let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query5);
                      //Update ticket exit number plate and image
                      let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query6);

                      g_exit_object.exit_flag = true;
                      g_entry_object.entry_image = "";
                      g_entry_object.number_plate = "";

                      //Insert into commands to clear
                      let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                      await client.query(query7);
                      //Insert into commands to display amount
                      let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                      await client.query(query8);
                      let cmd1 = {
                        amount: TotalCharge,
                        invoice_id: "0123456789",
                      };
                      cmd1 = JSON.stringify(cmd1);
                      //Insert into commands to trigger POS
                      let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                      await client.query(query34);
                      await new Promise((resolve) => setTimeout(resolve, 200));
                      //Insert commands to send money to POS
                      //Insert into commands
                      let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                      await client.query(query35);
                      Utils.logPublisher(
                        2,
                        1,
                        2,
                        `Customer has to Pay ${TotalCharge}`
                      );
                      /*
                        //New Payment Setup
                        //Insert into commands to trigger POS
                        let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','50')`;
                        await client.query(query34);
                        await new Promise((resolve) => setTimeout(resolve, 200));
                        //Insert commands to send money to POS
                        //Insert into commands
                        let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.spPayIP}','${edgeController.spPayPort}','51','${cmd1}')`;

                        */

                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        msg: "Done!",

                        id: `${data1.pass_id}`,
                      };
                      return ctx.body;
                    } else {
                      //Insert into commands for paymnet succesfull
                      let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                      await client.query(query6);
                      Utils.logPublisher(2, 1, 2, `Payment Successful`);
                      //Update Reservation exit number plate - Completed
                      let query11 = `update reservations set exit_number_plate='${g_exit_object.number_plate}', status_id = 2, payment_status_id = 4, end_time = now()  where pass_id='${data1.pass_id}'`;
                      await client.query(query11);
                      //Update ticket exit number plate and image
                      let query12 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query12);

                      //Insert into commands to open barrier
                      let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                      await client.query(query3);

                      console.log("Car exiting with 15 min");
                      //Return
                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        msg: "Car exiting with 15 min!",

                        id: `${data1.pass_id}`,
                      };
                      return ctx.body;
                    }
                  } else {
                    console.log("Exit time present", temp1.exit_time);
                    let end_time = new Date(temp1.exit_time).getTime();
                    let new_end_time = new Date().getTime();
                    //cal diff between current end time and exit time
                    const timeDiff = new_end_time - end_time;
                    console.log(timeDiff);
                    // get sec
                    let sec = timeDiff / 1000;
                    console.log(sec);
                    if (sec > 300) {
                      // get hours
                      let hh =
                        Math.ceil(timeDiff / 1000 / 60 / 60) > 0
                          ? Math.ceil(timeDiff / 1000 / 60 / 60)
                          : 1;
                      const parkingCharges = (hh * temp1.amount).toFixed(2);

                      exit_hour = moment(end_time).add(hh, "hours").format();
                      //cal tax charges for parking charge
                      const taxCharges = (
                        parseFloat(parkingCharges) * 0.15
                      ).toFixed(2);
                      //Add tax charge and parking charge
                      const TotalCharge = (
                        parseFloat(parkingCharges) + parseFloat(taxCharges)
                      ).toFixed(2);
                      console.log(
                        `Total time ${hh} and charges cal is ${TotalCharge}`
                      );

                      //Insert into commands to display amount
                      let cmd = {
                        text: `Please Pay ${TotalCharge} SAR`,
                        invoice_id: Math.round(new Date().getTime() / 1000),
                      };
                      cmd = JSON.stringify(cmd);

                      //Update Reservation exit number plate - Completed
                      let query5 = `update reservations set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query5);
                      //Update ticket exit number plate and image
                      let query6 = `update tickets set exit_number_plate='${g_exit_object.number_plate}' where pass_id='${data1.pass_id}'`;
                      await client.query(query6);

                      g_exit_object.exit_flag = true;
                      g_entry_object.entry_image = "";
                      g_entry_object.number_plate = "";

                      //Insert into commands to clear
                      let query7 = `INSERT INTO commands (device_id, ip, port, cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','37')`;
                      await client.query(query7);
                      //Insert into commands to display amount
                      let query8 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','30', '${cmd}')`;
                      await client.query(query8);
                      let cmd1 = {
                        amount: TotalCharge,
                        invoice_id: "0123456789",
                      };
                      cmd1 = JSON.stringify(cmd1);
                      //Insert into commands to trigger POS
                      let query34 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','50')`;
                      await client.query(query34);
                      await new Promise((resolve) => setTimeout(resolve, 200));
                      //Insert commands to send money to POS
                      //Insert into commands
                      let query35 = `INSERT INTO commands (device_id, ip, port,cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','51','${cmd1}')`;
                      await client.query(query35);
                      Utils.logPublisher(
                        2,
                        1,
                        2,
                        `Customer has to Pay ${TotalCharge}`
                      );
                    } else {
                      //Insert into commands for paymnet succesfull
                      let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
                      await client.query(query6);
                      Utils.logPublisher(2, 1, 2, `Payment Successful`);

                      //Insert into commands to open barrier
                      let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
                      await client.query(query3);
                      Utils.logPublisher(2, 1, 2, `Barrier oppened`);

                      console.log("Payment is captured");

                      //Return
                      ctx.status = 200;
                      ctx.body = {
                        success: true,
                        msg: "Payment is captured!",
                      };
                      return ctx.body;
                    }
                  }
                } catch (error) {
                  console.log("Error", error);
                }
              } else {
                console.log("No ticket has been created");
                try {
                  //Invalid ticket command
                  let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
                  await client.query(query3);
                } catch (err) {
                  console.log(err);
                }
                ctx.status = 200;
                ctx.body = {
                  success: true,
                  msg: "No ticket has been created!",
                };
                return ctx.body;
              }
            } catch (error) {
              console.log(error);
            }
          } else {
            console.log("Invalid reservation status !", input.pass_id);
            try {
              //Invalid ticket command
              let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
              await client.query(query3);

              await new Promise((resolve) => setTimeout(resolve, 3000));

              //Scan Ticket
              let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
              await client.query(query4);
            } catch (err) {
              console.log(err);
            }
            ctx.status = 200;
            ctx.body = {
              success: true,
              msg: "Invalid reservation status !",
            };
            return ctx.body;
          }
        } else {
          console.log(
            "No On going Reservation found for passId !",
            input.pass_id
          );
          try {
            //Invalid ticket command
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','35')`;
            await client.query(query3);

            await new Promise((resolve) => setTimeout(resolve, 3000));

            //Scan Ticket
            let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
            await client.query(query4);
          } catch (err) {
            console.log(err);
          }
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "No On going Reservation found for passId !",
          };
          return ctx.body;
        }
      }
    }
  };
  //Send commands
  static sendCommands = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    let ip = "";
    let port = "";
    if (input.gate == "in") {
      ip = edgeController.entryIP;
      port = edgeController.entryPort;
    } else if (input.gate == "out") {
      ip = edgeController.exitIP;
      port = edgeController.exitPort;
    } else {
      console.log("Unable to process commands");
      return false;
    }
    //Open Barrier
    if (input.cmd_type == "10") {
      //Insert into commands to open barrier
      let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${ip}','${port}','10')`;
      await client.query(query3);
      console.log("Open Barrier command inserted!");
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Done!",
      };
      return ctx.body;
    } else if (input.cmd_type == "11") {
      //Insert into commands
      let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${ip}','${port}','11')`;
      await client.query(query4);
      console.log("Close Barrier command inserted!");
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Done!",
      };
      return ctx.body;
    } else if (input.cmd_type == "30") {
      //Insert into commands to display <scan ticket/press button>
      let cmd = {
        text: input.text,
      };
      cmd = JSON.stringify(cmd);
      //Insert into commands to display amount
      let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('123', '${ip}','${port}','30', '${cmd}')`;
      await client.query(query6);
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Done!",
      };
      return ctx.body;
    } else {
      //Insert into commands
      let query5 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('123', '${ip}','${port}','${input.cmd_type}')`;
      await client.query(query5);
      ctx.status = 200;
      ctx.body = {
        success: true,
        msg: "Done!",
      };
      return ctx.body;
    }
  };
  //Regular Parking
  static regularParking = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("7 =========> REGULAR PARKING DATA", JSON.stringify(input));
    try {
      //Generate passId with current time
      const passId = new Date().valueOf();
      const data = {
        customer_id: input.customer_id || null,
        vehicle_id: input.vehicle_id || null,
        site_parking_slot_id: config.site_parking_slot_id,
        reservation_type_id: config.reservation_type_id,
        end_time: null,
        site_id: config.site_id,
        status_id: config.status_id,
        pass_id: passId.toString(),
      };

      if (g_entry_object.entry_flag == false) {
        //Insert reservation
        let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount, number_plate) 
          VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100, '${g_entry_object.number_plate}')`;
        await client.query(query);
        //Get Reservation Details
        const query1 = `select * from reservations where pass_id='${data.pass_id}'`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (res.rows.length > 0) {
          try {
            //Insert ticket
            let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, number_plate,entry_time, pass_id, status_id)
            VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), '${g_entry_object.number_plate}', now(),'${data1.pass_id}', 4)`;
            Utils.logPublisher(2, 1, 1, `Ticket Generated : ${data1.pass_id}`);
            await client.query(query2);
            let cmd = {
              url: `https://demo.website.sp.hyperthings.in/ticket/${data1.pass_id}`,
              pass_id: data1.pass_id,
            };
            cmd = JSON.stringify(cmd);

            //Insert Command to print tickets
            let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
            Utils.logPublisher(2, 1, 1, `Ticket Printed`);

            await client.query(query6);

            await new Promise((resolve) => setTimeout(resolve, 500));
            //Insert into commands to open barrier
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
            await client.query(query3);
            Utils.logPublisher(2, 1, 1, `Barrier oppened`);
            //Insert commands to close barrier after 1 minute
            // setTimeout(async () => {
            //   //Insert into commands
            //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
            //   await client.query(query4);
            // }, 3000);
            //Update Reservation Status
            let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
            await client.query(query5);
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              // msg: `Ticket and Command has been generated - ${data.pass_id}`,
              msg: `${data.pass_id}`,
            };
            g_entry_object.entry_flag = true;
            return ctx.body;
          } catch (err) {
            console.log(err);
            console.log("Unable to create ticket!");
            ctx.status = 404;
            ctx.body = {
              success: false,
              msg: "Unable to create ticket!",
            };
            return ctx.body;
          }
        } else {
          console.log("No Reservation found for passId !", data.pass_id);
        }
      } else {
        //Insert reservation
        let query = `INSERT INTO reservations (start_time, reservation_type_id, status_id, site_id, pass_id, amount) 
          VALUES (now(), '${data.reservation_type_id}', '${data.status_id}', '${data.site_id}','${data.pass_id}', 100)`;

        await client.query(query);
        //Get Reservation Details
        const query1 = `select * from reservations where pass_id='${data.pass_id}'`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (res.rows.length > 0) {
          try {
            //Insert ticket
            let query2 = `INSERT INTO tickets (reservation_id, site_id, vehicle_id, entry_time, pass_id, status_id)
            VALUES ('${data1.id}', '${data1.site_id}', cast(NULLIF('${data.vehicle_id}', 'null') as int4), now(),'${data1.pass_id}', 4)`;
            await client.query(query2);
            Utils.logPublisher(
              2,
              1,
              1,
              `Ticket Generated : ${JSON.stringify(data1.pass_id)}`
            );

            let cmd = {
              url: `https://demo.website.sp.hyperthings.in/ticket/${data1.pass_id}`,
              pass_id: data1.pass_id,
            };
            cmd = JSON.stringify(cmd);

            //Insert Command to print tickets
            let query6 = `INSERT INTO commands (device_id, ip, port, cmd_type, data) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','20', '${cmd}')`;
            await client.query(query6);
            Utils.logPublisher(2, 1, 1, `Ticket Printed`);
            await new Promise((resolve) => setTimeout(resolve, 500));
            //Insert into commands to open barrier
            let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.entryIP}','${edgeController.entryPort}','10')`;
            await client.query(query3);
            Utils.logPublisher(2, 1, 1, `Barrier oppened`);
            //Insert commands to close barrier after 1 minute
            // setTimeout(async () => {
            //   //Insert into commands
            //   let query4 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '192.168.0.99','8008','11')`;
            //   await client.query(query4);
            // }, 3000);
            //Update Reservation Status
            let query5 = `update reservations set status_id = 5 where pass_id='${data.pass_id}'`;
            await client.query(query5);
            //Return
            ctx.status = 200;
            ctx.body = {
              success: true,
              // msg: `Ticket and Command has been generated - ${data.pass_id}`,
              msg: `${data.pass_id}`,
            };
            g_entry_object.entry_flag = true;
            return ctx.body;
          } catch (err) {
            console.log(err);
            console.log("Unable to create ticket!");
            ctx.status = 404;
            ctx.body = {
              success: false,
              msg: "Unable to create ticket!",
            };
            return ctx.body;
          }
        } else {
          console.log("No Reservation found for passId !", data.pass_id);
        }
      }
    } catch (error) {
      console.log(error);
      console.log("Unable to create reservation!");
      ctx.status = 404;
      ctx.body = {
        success: false,
        msg: "Unable to create reservation!",
      };
      return ctx.body;
    }
  };
  //Capture payments
  static capturePayment = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("Capture Payment", input);
    //if pass_id present
    if (input.status == 1) {
      try {
        //Call Ticket generation API
        // fetch tickets/reservation
        const query1 = `select * from reservations where pass_id='${input.passId}'`;
        const res = await client.query(query1);
        const data = res.rows[0];
        if (res.rows.length > 0) {
          //Insert into commands for paymnet succesfull
          let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
          await client.query(query6);
          Utils.logPublisher(2, 1, 2, `Payment Successful`);

          //Update Reservation Status
          let query5 = `update reservations set payment_status_id = 1, end_time = '${exit_hour}', invoice_id = ${input.recepit_no} where pass_id='${input.passId}'`;
          await client.query(query5);
          //await new Promise(resolve => setTimeout(resolve, 2000));

          //Update Tickets Status
          let query9 = `update tickets set exit_time = '${exit_hour}' where pass_id='${input.passId}'`;
          await client.query(query9);

          //Update Payment
          let query8 = `update payments set transaction_id = ${input.transaction_no}, invoice_id = ${input.recepit_no} where pass_id='${input.passId}'`;
          await client.query(query8);

          //Insert into commands to open barrier
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
          await client.query(query3);

          Utils.logPublisher(2, 1, 2, `Barrier oppened`);

          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Payment is captured!",
          };
          return ctx.body;
        } else {
          console.log("Pass ID does not exist!", input);
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Pass ID does not exist!",
          };
          return ctx.body;
        }
      } catch (err) {
        console.log(err);
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Unable to capture payment!",
        };
        return ctx.body;
      }
    } else if (input.status == 0) {
      try {
        //Call Ticket generation API
        // fetch tickets/reservation
        console.log("Input data for capture payment", input);
        const query1 = `select * from reservations where pass_id='${input.passId}'`;
        const res = await client.query(query1);
        const data1 = res.rows[0];
        if (res.rows.length > 0) {
          //Insert into commands to failed payment status
          // let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','36')`;
          // await client.query(query6);

          //Insert into commands to failed payment status
          let query6 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','32')`;
          await client.query(query6);
          Utils.logPublisher(2, 1, 2, `Payment Successful`);
          //Update Reservation Status
          // let query5 = `update reservations set payment_status_id = 4 where pass_id='${input.passId}'`;
          // await client.query(query5);
          console.log(exit_hour);

          //Update Reservation Status
          let query5 = `update reservations set payment_status_id = 1, end_time = '${exit_hour}' where pass_id='${input.passId}'`;
          await client.query(query5);

          //Update Tickets Status
          let query9 = `update tickets set exit_time = '${exit_hour}' where pass_id='${input.passId}'`;
          await client.query(query9);

          //Insert into commands to open barrier
          let query3 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','10')`;
          await client.query(query3);

          //await new Promise(resolve => setTimeout(resolve, 2000));

          //Insert into commands to open barrier
          // let query7 = `INSERT INTO commands (device_id, ip, port,cmd_type) VALUES ('${data1.site_id}', '${edgeController.exitIP}','${edgeController.exitPort}','34')`;
          // await client.query(query7);

          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Payment Failed to captured!",
          };
          return ctx.body;
        } else {
          console.log("Pass ID does not exist!", input);
          //Return
          ctx.status = 200;
          ctx.body = {
            success: true,
            msg: "Pass ID does not exist!",
          };
          return ctx.body;
        }
      } catch (err) {
        console.log(err);
        ctx.status = 200;
        ctx.body = {
          success: true,
          msg: "Unable to capture payment!",
        };
        return ctx.body;
      }
    }
  };
  //ANPR OnCarHandled
  static anprOnCarHandled = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("3 =========> ANPR anprOnCarHandled", JSON.stringify(input));
    const address = ctx.socket.remoteAddress;
    console.log(address);
    const regex = new RegExp(/\d{3,4}[A-Z]{3}$/);
    const license_no1 = regex.test(input.license);
    console.log(license_no1);

    if (address == "127.0.0.2") {
      try {
        g_entry_object = {
          number_plate: input.license,
          entry_image: null,
          entry_flag: false,
        };

        carId = input.carId;

        //get the no plate url
        //let url = await ftp(input.carId);

        // g_entry_object = {
        //   number_plate: input.license,
        //   entry_image: url,
        //   entry_flag: false,
        // };

        console.log(g_entry_object);

        //Return
        ctx.status = 200;
        ctx.body = {
          success: true,
        };
        return ctx.body;
      } catch (error) {
        console.log("Error", error);
        //Return
        //Return
        ctx.status = 500;
        ctx.body = {
          success: false,
        };
        return ctx.body;
      }
    } else if (address == "127.0.0.1") {
      try {
        g_exit_object = {
          number_plate: input.license,
          exit_image: null,
          exit_flag: false,
        };

        carId = input.carId;
        //get the no plate url
        //let url = await ftp(input.carId);

        // g_exit_object = {
        //   number_plate: input.license,
        //   exit_image: url,
        //   exit_flag: false,
        // };

        console.log(g_exit_object);

        //Return
        ctx.status = 200;
        ctx.body = {
          success: true,
        };
        return ctx.body;
      } catch (error) {
        console.log("Error", error);
        //Return
        ctx.status = 200;
        ctx.body = {
          success: true,
        };
        return ctx.body;
      }
    } else {
      console.log("Incorrect Ip or number plate data not recived");
      //Return
      ctx.status = 200;
      ctx.body = {
        success: true,
      };
      return ctx.body;
    }
  };
  //ANPR OnCarArrival
  static anprOnCarArrival = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("2 =========> ANPR anprOnCarArrival", JSON.stringify(input));
    //Return
    ctx.status = 200;
    ctx.body = {
      success: true,
    };
    return ctx.body;
  };
  //ANPR OnCarDeparture
  static anprOnCarDeparture = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("3 =========> ANPR anprOnCarDeparture", JSON.stringify(input));
    return;
    const address = ctx.socket.remoteAddress;
    console.log(address);
    const regex = new RegExp(/\d{3,4}[A-Z]{3}$/);
    const license_no1 = regex.test(input.license);
    //console.log(license_no1);
    if (address == "192.168.0.40") {
      try {
        let number_plate = input.license;

        //get the no plate url
        let url = await ftp(input.carId);
        let data1 = {
          url: url,
        };
        data1 = JSON.stringify(data1);

        //Get latest entry_time data
        const query = "SELECT * FROM tickets ORDER BY entry_time DESC LIMIT 1";
        const res = await client.query(query);
        const data = res.rows[0];

        //update Nuber_plate for latest entry_time data
        let query2 = `UPDATE tickets 
                SET
                number_plate= '${number_plate}', entry_image = '${data1}'
                WHERE id=${data.id}
               `;
        await client.query(query2);

        //update reservation with number_plate
        let query4 = `UPDATE reservations 
                SET
                number_plate= '${number_plate}'
                WHERE id=${data.reservation_id}
              `;
        await client.query(query4);

        console.log(
          `Updated the no_plate: ${number_plate} for reservation: ${data.reservation_id} and tickets: ${data.id}`
        );

        // //Return
        // response
        //   .status(200)
        //   .json(
        //     `Updated the no_plate: ${number_plate} for reservation: ${data.reservation_id} and tickets: ${data.id}`
        //   );
      } catch (error) {
        console.log("Error", error);
        //response.status(404).json(error);
      }
    } else if (address == "192.168.0.41") {
      try {
        let number_plate = input.license;

        //get the no plate url
        let url = await ftp(input.carId);
        let data1 = {
          url: url,
        };
        data1 = JSON.stringify(data1);

        //Get latest entry_time data
        const query = `SELECT * FROM tickets WHERE number_plate= '${number_plate}'`;
        const res = await client.query(query);
        const data = res.rows[0];

        //update Nuber_plate for latest entry_time data
        let query2 = `UPDATE tickets 
                SET
                exit_number_plate= '${number_plate}', exit_image = '${data1}'
                WHERE id=${data.id}
               `;
        await client.query(query2);

        //update reservation with number_plate
        let query4 = `UPDATE reservations 
                SET
                exit_number_plate= '${number_plate}'
                WHERE id=${data.reservation_id}
              `;
        await client.query(query4);

        console.log(
          `Updated the exit no_plate: ${number_plate} for reservation: ${data.reservation_id} and tickets: ${data.id}`
        );

        //Return
        // response
        //   .status(200)
        //   .json(
        //     `Updated the exit no_plate: ${number_plate} for reservation: ${data.reservation_id} and tickets: ${data.id}`
        //   );
      } catch (error) {
        console.log("Error", error);
        //response.status(404).json(error);
      }
    } else {
      console.log("Incorrect Ip or number plate data recived");
      //response.status(500).json("Incorrect Ip or number plate data recived");
    }
  };
  //ANPR OnNotification
  static anprOnNotification = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("4 =========> ANPR OnNotification", JSON.stringify(input));
    //Return
    ctx.status = 200;
    ctx.body = {
      success: true,
    };
    return ctx.body;
  };
  //ANPR OnInputChange
  static anprOnInputChange = async (ctx, next) => {
    //Get request body
    const input = ctx.request.body;
    console.log("5 =========> ANPR OnInputChange", JSON.stringify(input));
    //Return
    ctx.status = 200;
    ctx.body = {
      success: true,
    };
    return ctx.body;
  };
};
