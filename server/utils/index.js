//Import Axios
const Axios = require("axios");
//Import mqtt
const mqtt = require("mqtt");
//Import moment
const moment = require("moment");
//Import Settings
const { mqttUrl, mqttTopic } = require("../config/adaptor");
//connect to mqtt client
const client = mqtt.connect(mqttUrl);

//All Utils
class Utils {
  //log Publisher
  async logPublisher(servName, logLevel, source, msg) {
    // console.log("Here!!");
    try {
      if (servName && logLevel && source && msg) {
        if (servName == 1) {
          servName = "TCP-server";
        } else {
          servName = "unknown!";
        }
        if (logLevel == 1) {
          logLevel = "Success";
        } else if (logLevel == 2) {
          logLevel = "Error";
        } else if (logLevel == 3) {
          logLevel = "Crashed";
        } else {
          logLevel = "unknown!";
        }
        if (source == 1) {
          source = "EntryGate";
        } else if (source == 2) {
          source = "ExitGate";
        } else if (source == 3) {
          source = "server";
        } else if (source == 4) {
          source = "DB";
        } else {
          source = "unknown";
        }
        let pubObj = {};
        pubObj.time = moment().format();
        pubObj.serviceNeme = servName;
        pubObj.logLevel = logLevel;
        pubObj.source = source;
        pubObj.mgs = msg;
        // console.log(pubObj);

        await this.pushdata(mqttTopic, JSON.stringify(pubObj));
      }
    } catch (error) {
      console.log("cannot capture log", error);
    }
  }

  pushdata(topic, data) {
    return new Promise((resolve) => {
      client.publish(topic, data);
      resolve("push data");
    });
  }
  //Reverse hex bytes
  revHex(str) {
    return str
      .match(/[a-fA-F0-9]{2}/g)
      .reverse()
      .join("");
  }
  // converting hex to binary
  hex2bin(hex) {
    return ("00000000" + parseInt(hex, 16).toString(2)).substr(-8);
  }
  //Hex to string // ASCII
  h2a(str) {
    console.log(str);
    const hex = str.toString();
    let result = "";
    for (var n = 0; n < hex.length; n += 2) {
      result += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return result;
  }
  //Hex to string // ASCII
  h22a(arr) {
    let parserTest = [];
    arr.forEach((element) => {
      const hex = element.toString();
      let result = "";
      for (let n = 0; n < hex.length; n += 2) {
        result += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
      }
      parserTest.push(result);
    });
    return parserTest;
  }
  //Compute Checksum
  computeChecksum(input) {
    let temp = "0000";
    for (let k = 0; k < input.length; k = k + 2) {
      const a = temp;
      const b = "00" + input.slice(k, k + 2);
      let ndigits = 4,
        i,
        carry = 0,
        d,
        result = "";

      for (i = ndigits - 1; i >= 0; i--) {
        d = parseInt(a[i], 16) + parseInt(b[i], 16) + carry;
        carry = d >> 4;
        result = (d & 15).toString(16) + result;
      }
      temp = result;
    }
    // console.log(`Checksum of -> ${input} is ${temp.slice(-2)}`);
    return temp.slice(-2);
  }
  //Compute XOR
  computeXor(input) {
    let xor_temp = "00";
    let xor_str = xor_temp;
    for (let i = 0; i < input.length; i = i + 2) {
      const buf1 = Buffer.from(xor_str, "hex");
      const buf2 = Buffer.from(input.slice(i, i + 2), "hex");
      const bufResult = buf1.map((b, i) => b ^ buf2[i]);
      xor_str = bufResult.toString("hex");
    }
    // console.log(`XOR of -> ${input} is ${xor_str}`);
    return xor_str;
  }
  //Save raw data logs
  async saveRawData(data) {
    //Logic
    try {
      //Save raw data log
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/raw-data`,
        headers: {},
        data,
      });
      return true;
    } catch (err) {
      console.log("32 =====> Error!", err);
      return false;
    }
  }
  // Init Ticket generator Queue
  async genTicket(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/entry-ticket-manager`,
        headers: {},
        data,
      });
      console.log("2=====> Response from Entry Ticket Manager", response.data);

      return response.data;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }
  //call Update ticket
  async updateTicket(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/update-ticket`,
        headers: {},
        data,
      });
      console.log("2=====> Response from update-ticket", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }

  //call exit update
  async exitUpdateTicket(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/exit_update_ticket`,
        headers: {},
        data,
      });
      console.log("2=====> Response from exit_update_ticket", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }

  //Call Exit ticket manager
  async closeTicket(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/exit-ticket-manager`,
        headers: {},
        data,
      });
      console.log("2=====> Response from Exit Ticket Manager", response.data);
      return response.data;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }
  //Send Commands
  async sendCommands(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/command-manager`,
        headers: {},
        data,
      });
      console.log("2=====> Response from Command Manager", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }
  // Init Regular parking
  async genRegularParking(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/regular-parking`,
        headers: {},
        data,
      });
      console.log("2=====> Response from Regular Parking", response.data);
      return { status: true, pass_id: response.data.msg };
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }

  async posParser(input) {
    const data = input.split("1c");
    const temp = [];
    temp.push(data[1], data[3], data[11]);
    const parserTest = this.h22a(temp);
    const posStatus = {
      status: parserTest[0],
      recepit_no: parserTest[1],
      transaction_no: parserTest[2],
    };
    return posStatus;
  }

  async revHex(str) {
    return str

      .match(/.{1,2}/g)

      .reverse()

      .join("");
  }

  //capture payment
  async capturePayment(data) {
    //Logic
    try {
      //Send request to Data sharing API with tokens
      const response = await Axios({
        method: "POST",
        url: `http://localhost:9001/payments`,
        headers: {},
        data,
      });
      console.log("2=====> Response from payments", response.data);
      return true;
    } catch (err) {
      console.log("3 =====> Error!", err);
      return false;
    }
  }
}
module.exports = Utils;
