module.exports = {
  appEnv: process.env.NODE_ENV || "dev",
  appLog: process.env.APP_LOG || "dev",
  appPort: process.env.SP_TICKET_MANAGER_PORT || 9001,
  appHost: "0.0.0.0",
  pgdbHost: process.env.SP_EDGE_PGDB_HOST || "localhost",
  pgdbPort: process.env.SP_EDGE_PGDB_PORT || "54323",
  pgdbIsAuth: process.env.SP_EDGE_PGDB_IS_AUTH || "true",
  pgdbUsername: process.env.SP_EDGE_PGDB_USERNAME || "master",
  pgdbPassword: process.env.SP_EDGE_PGDB_PASSWORD || "DHNNOQIYWMDZZPOQ",
  pgDbName: process.env.SP_EDGE_PGDB_NAME || "EV3",
  site_parking_slot_id: process.env.SITE_PARKING_SLOT_ID || 506,
  reservation_type_id: process.env.RESERVATION_TYPE_ID || 23,
  site_id: process.env.SITE_ID || 12,
  status_id: process.env.STATUS_ID || 1,
  entryControllerIP: process.env.SP_ENTRY_CONTROLLER_IP || "192.168.0.99",
  exitControllerIP: process.env.SP_EXIT_CONTROLLER_IP || "127.0.0.1",
  entryControllerPort: process.env.SP_ENTRY_CONTROLLER_PORT || 8008,
  exitControllerPort: process.env.SP_EXIT_CONTROLLER_PORT || 8008,
  //Minio Connection
  url:
    process.env.SP_MINIO_URL || process.env.MINIO_URL || "iot.hyperthings.in",
  accessKey:
    process.env.SP_MINIO_ACCESSKEY ||
    process.env.MINIO_ACCESSKEY ||
    "xcHwoEOYiOYDksDz",
  secretKey:
    process.env.SP_MINIO_SECRETKEY ||
    process.env.MINIO_SECRETKEY ||
    "nAZSIkmTHnxDlyeY",
  port: process.env.SP_MINIO_PORT || process.env.MINIO_PORT || 17208,
  bucket_name:
    process.env.SP_MINIO_BUCKET_NAME || process.env.MINIO_BUCKET_NAME || "pdfs",
  publicUrl: "https://cdn.hyperthings.in",
  mqttUrl: process.env.MQTTURL || "mqtt://iot.hyperthings.in:17004",
  mqttTopic: process.env.MQTT_TOPIC || "915-test-topic1",
};
