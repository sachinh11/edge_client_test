const net = require('net');
//Import Node PG
const { Client } = require('pg');
//Import Events
const EventEmitter = require('events');
//Import Util
const util = require('util');
//Import Utility functions
const UtilClass = require('./utils');
const Utils = new UtilClass();
//Import Configs
const {
  spdb,
  entryControllerIP,
  exitControllerIP,
} = require('./config/adaptor');

(async () => {
  try {
    const dbclient = new Client(spdb.pg.uri);

    try {
      await dbclient.connect();
      console.log('DataBase connected!');
    } catch (e) {
      console.log('DataBase Not connected', e);
    }
    // Build and instantiate our custom event emitter
    function DbEventEmitter() {
      EventEmitter.call(this);
    }

    util.inherits(DbEventEmitter, EventEmitter);

    const dbEventEmitter = new DbEventEmitter();

    // Handle New devices
    dbEventEmitter.on('new_command', async (msg) => {
      // Fetch command
      let new_cmd = await dbclient.query(
        `select * from commands where id = ${msg.id}`
      );
      //Create the client using Socket method.
      const client = new net.Socket();
      new_cmd = new_cmd.rows[0];
      console.log(new_cmd.cmd_type);

      //Now that client is created, let's make it connect to the server and send some message. So that server can echo back the same to all clients
      client.connect(new_cmd.port, new_cmd.ip, async () => {
        console.log(`client connected to ${new_cmd.ip}:${new_cmd.port}`);
        let cmd_str = '';
        if (new_cmd.cmd_type == '10') {
          //Open Barrier
          cmd_str = '7F01004404010120031DED';
          console.log('Open Barrier', cmd_str);
        } else if (new_cmd.cmd_type == '111') {
          await new Promise((resolve) => setTimeout(resolve, 2000));
          //Close Barrier
          cmd_str = '7F02004404020120031DEF';
          console.log('Close Barrier', cmd_str);
        } else if (new_cmd.cmd_type == 20) {
          //Convert pass_id to hex
          let pass_id_hex = Utils.asciiToHex(new_cmd.data.pass_id);
          let temp_0 = '';
          for (let i = 0; i < 48 - pass_id_hex.length; i++) {
            temp_0 = temp_0 + '0';
          }
          pass_id_hex += temp_0;
          //Hex of URL
          let url_hex = Utils.asciiToHex(new_cmd.data.url);
          url_hex += '00';
          cmd_str = `7F03007154${pass_id_hex}${url_hex}`;
          const xor = Utils.computeXor(cmd_str);
          const checksum = Utils.computeChecksum(cmd_str);
          cmd_str += xor + checksum;
          console.log('Print ticket', cmd_str);
        }
        //Display text
        else if (new_cmd.cmd_type == 30) {
          //Convert text to hex
          let text_hex = Utils.asciiToHex(new_cmd.data.text);

          text_hex = `0050${text_hex}00`;

          let text_hex_len = text_hex.length / 2;
          text_hex_len = text_hex_len.toString(16);
          cmd_str = `7F030073${text_hex_len}${text_hex}`;
          const xor = Utils.computeXor(cmd_str);
          const checksum = Utils.computeChecksum(cmd_str);
          cmd_str += xor + checksum;
          console.log('Display Command', cmd_str);
        } else if (new_cmd.cmd_type == 50) {
          cmd_str = '7F 01 00 19 05 02 02050304    60 AE';
          console.log('POS Command trigger', cmd_str);
        } else if (new_cmd.cmd_type == 51) {
          const pos_text = Utils.computePosCmd({
            MSGID: 'PUR',
            ECRNO: '001',
            RecieptNo: new_cmd.data.invoice_id,
            Amount: new_cmd.data.amount,
          });
          cmd_str = '7F0300192602' + pos_text;
          cmd_str =
            cmd_str +
            Utils.computeXor(cmd_str) +
            Utils.computeChecksum(cmd_str);
          console.log('POS Money Command', cmd_str);
        } else if (new_cmd.cmd_type == 31) {
          //Home Page
          cmd_str = '7F0300740206000CFE';
          console.log('Home Page', cmd_str);
        } else if (new_cmd.cmd_type == 32) {
          //Payment Successful
          cmd_str = '7F0300740201000Bf9';
          console.log('Payment Success', cmd_str);
        } else if (new_cmd.cmd_type == 33) {
          //scan the ticket /press button
          cmd_str = '7F03007402020008FA';
          console.log('Scan Or Press', cmd_str);
        } else if (new_cmd.cmd_type == 34) {
          //scan the ticket
          cmd_str = '7F03007402030009FB';
          console.log('Scan Ticket', cmd_str);
        } else if (new_cmd.cmd_type == 35) {
          //Invalid Ticket
          cmd_str = '7F0300740205000FFD';
          console.log('Invalid Ticket', cmd_str);
        } else if (new_cmd.cmd_type == 36) {
          //Payment UnSuccessful
          cmd_str = '7F0300740204000EFC';
          console.log('Payment Failed', cmd_str);
        } else if (new_cmd.cmd_type == 37) {
          //Clear Before payment
          cmd_str = '7F0300740200000AF8';
          console.log('Payment Clear', cmd_str);
        }
        //Reset Device
        else if (new_cmd.cmd_type == 89) {
          //Reset Device
          cmd_str = '7F400001003EC0';
          console.log('Reset cmd', cmd_str);
        }
        //Get Version
        else if (new_cmd.cmd_type == 88) {
          cmd_str = '7F310002004CB2';
          console.log('Get Version cmd', cmd_str);
        } else {
          console.log('Unsupported command type', new_cmd.cmd_type);
          return false;
        }
        //Send Command
        const bufferRes = Buffer.from(cmd_str, 'hex');
        const res = client.write(bufferRes);
        console.log('Response from controller', res);
      });
      //Lets listen to some events similar to server like data, close and error.
      client.on('data', (rawData) => {
        const data = Buffer.from(rawData).toString('hex');
        console.log(`Client received: ${data}`);
        client.destroy();
      });
      // Add a 'close' event handler for the client socket
      client.on('close', () => {
        console.log('Client closed');
      });
      client.on('error', (err) => {
        console.error(err);
      });
    });
    // Listen for all pg_notify channel messages
    dbclient.on('notification', function (msg) {
      let payload = JSON.parse(msg.payload);
      dbEventEmitter.emit(msg.channel, payload);
    });

    // Designate which channels we are listening on. Add additional channels with multiple lines.
    await dbclient.query('LISTEN new_command');
  } catch (e) {
    console.log(e);
  }
})();
