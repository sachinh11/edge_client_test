module.exports = {
  appEnv: process.env.NODE_ENV || 'dev',
  appLog: process.env.APP_LOG || 'dev',
  appPort: process.env.SP_EDGE_TCP_HANDLER || 9000,
  appHost: '0.0.0.0',
  pgdbHost: process.env.SP_EDGE_PGDB_HOST || 'localhost',
  pgdbPort: process.env.SP_EDGE_PGDB_PORT || '5432',
  pgdbIsAuth: process.env.SP_EDGE_PGDB_IS_AUTH || 'true',
  pgdbUsername: process.env.SP_EDGE_PGDB_USERNAME || 'postgres',
  pgdbPassword: process.env.SP_EDGE_PGDB_PASSWORD || 'hyperthings@123',
  pgDbName: process.env.SP_EDGE_PGDB_NAME || 'hts-edge-pf',
  entryControllerIP: process.env.SP_ENTRY_CONTROLLER_IP || '192.168.0.99',
  exitControllerIP: process.env.SP_EXIT_CONTROLLER_IP || '192.168.0.100',
};
