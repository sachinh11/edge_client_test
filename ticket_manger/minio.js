var Minio = require("minio");
const fs = require("fs");
const config = require("./config/adaptor");

// Instantiate the minio client with the endpoint
// and access keys as shown below.

var minioClient = new Minio.Client({
  endPoint: "iot.hyperthings.in",
  port: 17208,
  useSSL: false,
  accessKey: "xcHwoEOYiOYDksDz",
  secretKey: "nAZSIkmTHnxDlyeY",
});

// minioClient = new Minio.Client({
//   endPoint: "play.min.io",
//   port: 9000,
//   useSSL: true,
//   accessKey: "Q3AM3UQ867SPQQA43P2F",
//   secretKey: "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
// });

var metaData = {
  "Content-Type": "application/octet-stream",
  "X-Amz-Meta-Testing": 1234,
  example: 5678,
};

async function data(file) {
  return new Promise(async (resolve, rejects) => {
    //console.log(file, __dirname);
    var fileStream = `${__dirname}\\data\\${file}`;

    try {
      if (fs.existsSync(fileStream)) {
        console.log("File exists.");
        await minioClient.fPutObject(
          config.minio.bucket_name,
          file,
          fileStream
        );

        // const url = await minioClient.presignedGetObject(
        //   config.minio.bucket_name,
        //   file
        // );
        const url = `${config.minio.publicUrl}/${config.minio.bucket_name}/${file}`;

        fs.unlink(`${__dirname}\\data\\${file}`, function (err) {
          if (err) rejects(err);
          console.log("file deleted successfully");
        });

        resolve(url);
      } else {
        console.log("File does not exist.");
      }
    } catch (err) {
      console.error(err);
    }
  });
}

module.exports = data;
